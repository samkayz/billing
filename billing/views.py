from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib import messages
UserModel = get_user_model()
from user.models import *
import random
import string
import uuid
import datetime
import json
import requests
from django.contrib import messages
from datetime import datetime


def index(request):
    return render(request, 'home/index.html')


def about(request):
    return render(request, 'home/about.html')


def faq(request):
    return render(request, 'home/faq.html')


def contact(request):
    return render(request, 'home/contact.html')


def pricing(request):
    return render(request, 'home/pricing.html')


def pay_with_pin(request):
    U = 14
    res1 = ''.join(random.choices(string.digits, k=U))
    txn1 = str(res1)
    N = 6
    res2 = ''.join(random.choices(string.digits, k=N))
    txn2 = str(res2)
    c_user_id = request.user.id
    trans_ref = "WLTP|" + txn1 + "|" + txn2
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    U = 6
    res = ''.join(random.choices(string.digits, k=U))
    txn = str(res)
    txt_id = "TX" + txn
    if request.method == "POST":
        s_card = request.POST['s_card']
        email = request.POST['email']
        v_pin = request.POST['v_pin']
        v_exist = VirtualPin.objects.filter(pin=v_pin).exists()

        if not v_exist:
            messages.error(request, "Invalid Pin")
            return redirect('pay_with_pin')
        elif UsedPin.objects.filter(pin_code=v_pin).exists() and VirtualPin.objects.values('pin_status').get(pin=v_pin)['pin_status'] == 1:
            messages.error(request, "Pin has been used")
            return redirect('pay_with_pin')
        else:
            v_amt = VirtualPin.objects.values('pin_amount').get(pin=v_pin)['pin_amount']
            url = "https://api.ravepay.co/v2/services/confluence"

            payload = '{\n  \"secret_key\": \"FLWSECK_TEST-0b93cce86006c4f85d7af034d51169eb-X\",\n  \"service\": \"fly_buy\",\n  \"service_method\": \"post\",\n  \"service_version\": \"v1\",\n  \"service_channel\": \"rave\",\n  \"service_payload\": {\n    \"Country\": \"NG\",\n    \"CustomerId\": \"'+ s_card +'\",\n    \"Reference\": \"'+ txt_id +'\",\n    \"Amount\": '+ str(v_amt) +',\n    \"RecurringType\": 0,\n    \"IsAirtime\": false,\n    \"BillerName\": \"DSTV\"\n  }\n}'
            headers = {
            'Content-Type': 'application/json'
            }

            response = requests.request("POST", url, headers=headers, data = payload)
            print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                airt = d_dict[j]
                airtime = f.append(airt)
            air_resp = f[2]
            VirtualPin.objects.filter(pin=v_pin).update(pin_status="1")
            tx = Transactions(user_id=0, payment_ref=txt_id, payment_date=now, rec_email=email, 
            payment_desc="Dstv Payment", payment_method="VOUCHER", trans_ref=trans_ref, amount=v_amt, payment_status="PAID")

            u_pin = UsedPin(pin_code=v_pin, pin_amount=v_amt, smart_card=s_card, user_email=email, date_used=now)
            u_pin.save()
            tx.save()
            messages.success(request, "Transaction Successfull")
            return redirect('pay_with_pin')
    else:
        return render(request, 'pay_with_pin.html')


def payme(request, id):
    all_data = UserModel.objects.all().get(customer_id=id)
    return render(request, 'payme.html', {'all_data': all_data})


def pay(request):
    U = 6
    res = ''.join(random.choices(string.digits, k=U))
    txn = str(res)
    txt_id = "FX" + txn
    if request.method == "POST":
        r_email = request.POST['r_email']
        amount = request.POST['amount']
        s_name = request.POST['s_name']
        cur = request.POST['cur']
        charge = ((3.8/100) * float(amount))
        #print(charge)
        sc = ((1.2/100) * float(amount))
        t_charge = charge + sc
        total = float(amount) + t_charge
        #print(t_charge)
        ext = ReceiveMoneyForm.objects.filter(email=r_email).exists()
        if UserModel.objects.filter(email=r_email).exists():
            if ext:
                all_data = ReceiveMoneyForm.objects.all().get(email=r_email)
                if all_data.app_status == 0:
                    messages.error(request, "User can't receive money at the moment")
                    return redirect('pay')
                else:
                    return render(request, 'pay.html', {'all_data': all_data, 'amount': amount, 's_name': s_name, 'cur': cur, 'ext': ext, 'txt_id': txt_id})
            else:
                messages.error(request, "User can't receive money at the moment")
                return redirect('pay')
        else:
            messages.error(request, "User Account not found")
            return redirect('pay')
    else:
        return render(request, 'pay.html')


def pay_success(request):
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M"))
    if request.method == "GET":
        txn_id = request.GET.get('paymentReference')
        email = request.GET.get('email')
        amt = request.GET.get('amount')
        id = UserModel.objects.values('id').get(email=email)['id']
        bal = Account.objects.all().get(user_id=id)
        url = "https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify"

        payload = '{\n\t\"txref\" : \"' + txn_id + '\",\n\t\"SECKEY\" : \"FLWSECK_TEST-0b93cce86006c4f85d7af034d51169eb-X\"\n}'
        headers = {
        'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        r_dict = json.loads(response.text)
        
        e = []
        for i in r_dict:
            dt = r_dict[i]
            txn = e.append(dt)
        trans = e[2]
        #print(trans)
        amount = trans['amount']
        cur = trans['currency']
        c_amount = trans['chargedamount']
        txref = trans['txref']
        txid = trans['txid']
        flwref = trans['flwref']
        f_charge = trans['appfee']
        chargecode = trans['chargecode']
        paymenttype = trans['paymenttype']
        narration = trans['narration']
        sc = ((1.2/100) * float(amount))
        
        usd_bal = float(bal.usd)
        eur_bal = float(bal.eur)
        gbp_bal = float(bal.gbp)

        init_amt = float(amount) - sc
        if e[0] == "success":
            if chargecode == "00":
                if float(amount) == float(amt):
                    if cur == "USD":
                        usd = init_amt + usd_bal
                        Account.objects.filter(user_id=id).update(usd=usd)
                        txn = Transactions(user_id=id, payment_ref=txref, payment_date=now, rec_email=email, payment_desc=narration, 
                        payment_method=paymenttype, trans_ref=flwref, amount=init_amt, cur=cur, payment_status="SUCCESS")
                        txn.save()
                        return render(request, 'pay.html')
                    elif cur == "EUR":
                        eur = init_amt + eur_bal
                        Account.objects.filter(user_id=id).update(eur=eur)
                        txn = Transactions(user_id=id, payment_ref=txref, payment_date=now, rec_email=email, payment_desc=narration, 
                        payment_method=paymenttype, trans_ref=flwref, amount=init_amt, cur=cur, payment_status="SUCCESS")
                        txn.save()
                        return render(request, 'pay.html')
                    else:
                        gbp = init_amt + gbp_bal
                        Account.objects.filter(user_id=id).update(gbp=gbp)
                        txn = Transactions(user_id=id, payment_ref=txref, payment_date=now, rec_email=email, payment_desc=narration, 
                        payment_method=paymenttype, trans_ref=flwref, amount=init_amt, cur=cur, payment_status="SUCCESS")
                        txn.save()
                        return render(request, 'pay.html')
                else:
                    messages.error(request, "1")
                    return render(request, 'pay.html')
            else:
                messages.error(request, "2")
                return render(request, 'pay.html')
        else:
            messages.error(request, "3")
            return render(request, 'pay.html')
    else:
        return render(request, 'pay.html')
