from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response
from django.db.models import Q
from .serializers import *
from datetime import datetime
import random
import string
import uuid
import math
import requests
import json
from django.contrib.auth import get_user_model
from user.models import *
from django.contrib.auth import user_logged_in
from django.dispatch.dispatcher import receiver
UserModel = get_user_model()
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.utils.html import strip_tags
from django.db.models import Sum
from billing.settings import EMAIL_FROM

# Create your views here.


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def query(request, ref):
    try:
        show = Transactions.objects.all().get(payment_ref=ref)
    except Transactions.DoesNotExist:
        error = {
            "status": status.HTTP_400_BAD_REQUEST,
            "data": {
                "message": "Transaction doesn't exist"
            }
        }
        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)

    da = {
        "status": {
            "status": status.HTTP_200_OK
        },
        "data": {
        "payment_ref": show.payment_ref,
        "payment_date": show.payment_ref,
        "rec_email": show.rec_email,
        "payment_desc": show.payment_desc,
        "payment_method": show.payment_method,
        "trans_ref": show.trans_ref,
        "payment_status": show.payment_status,
        "amount": show.amount,
        "cur": show.cur
        }
    }
    return Response(data=da, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def verify(request):
    email = request.data.get('email')

    try:
        show = UserModel.objects.all().get(email=email)
    except UserModel.DoesNotExist:
        error = {
            "status": status.HTTP_400_BAD_REQUEST,
            "data": {
                "message": "User doesn't exist"
            }
        }
        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
    
    data = {
        "status": {
            "status": status.HTTP_200_OK
        },
        "data": {
            "first_name": show.first_name,
            "last_name": show.last_name,
            "phone": show.phone,
            "email": show.email
        }
    }
    return Response(data=data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def accountNo(request):
    c_id = request.user.id
    try:
        acctDetail = UserModel.objects.all().get(id=c_id)
    except UserModel.DoesNotExist:
        error = {
            "status": status.HTTP_400_BAD_REQUEST,
            "data": {
                "message": "User doesn't exist"
            }
        }
        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)

    if acctDetail.acct_type == 1:
        data = {
        "status": {
            "status": status.HTTP_200_OK
        },
        "accountNumber": acctDetail.acct_no,
        "accountName": acctDetail.acct_name,
        "bankName": acctDetail.bank_name,
        "bankCode": acctDetail.bank_code,
        "accountType": "personal",
        }

        return Response(data=data, status=status.HTTP_200_OK)
    else:
        data = {
        "status": {
            "status": status.HTTP_200_OK
        },
        "accountNumber": acctDetail.acct_no,
        "accountName": acctDetail.acct_name,
        "bankName": acctDetail.bank_name,
        "bankCode": acctDetail.bank_code,
        "accountType": "business"
        }

        return Response(data=data, status=status.HTTP_200_OK)
    


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def send_money(request):
    U = 14
    res1 = ''.join(random.choices(string.digits, k=U))
    txn1 = str(res1)
    N = 6
    res2 = ''.join(random.choices(string.digits, k=N))
    txn2 = str(res2)
    trans_ref = "WLTR|" + txn1 + "|" + txn2
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    H = 6
    res = ''.join(random.choices(string.digits, k=H))
    txn = str(res)
    txt_id = "TX" + txn
    c_id = request.user.id
    c_user_email = request.user.email
    c_user_name = request.user.last_name
    if request.method == 'POST':
        r_email = request.data.get('r_email')
        amount = request.data.get('amount')
        s_acct = request.data.get('s_acct')
        r_acct = request.data.get('r_acct')
        userExist = UserModel.objects.filter(email=r_email).exists()
        if userExist:
            r_name = UserModel.objects.values('last_name').get(email=r_email)['last_name']
            s_all = UserModel.objects.all().get(id=c_id)
            all_d = Account.objects.all().get(user_id=c_id)
            if request.user.email == r_email:
                error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                "message": "Sorry You can't send money to yourself"
                    }
                }
                return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
            else:
                if s_acct == "NGN" and r_acct == "NGN":
                    if (float(amount)) > all_d.bal:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.bal

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(amount) + r_bal.bal)
                        new2 = (s_bal - float(amount))

                        Account.objects.filter(user_id=r_info.id).update(bal=new1)
                        Account.objects.filter(user_id=c_id).update(bal=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                        "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "NGN/NGN/" + r_name,
                        "payment_method": "WALLET",
                        "trans_ref": trans_ref,
                        "amount": amount,
                        "cur": "NGN", 
                        "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "NGN/NGN/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "NGN", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "NGN" and r_acct == "USD":
                    if (float(amount)) > all_d.bal:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=NGN/USD&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.bal

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.usd)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(usd=new)
                        Account.objects.filter(user_id=c_id).update(bal=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "NGN/USD/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "NGN/USD/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "NGN", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "NGN" and r_acct == "EUR":
                    if (float(amount)) > all_d.bal:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=NGN/EUR&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.bal

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.eur)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(eur=new)
                        Account.objects.filter(user_id=c_id).update(bal=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "NGN/EUR/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "NGN/EUR/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "NGN", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "NGN" and r_acct == "GBP":
                    if (float(amount)) > all_d.bal:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=NGN/GBP&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.bal

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.gbp)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(gbp=new)
                        Account.objects.filter(user_id=c_id).update(bal=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "NGN/GBP/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "NGN/GBP/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "NGN", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "USD" and r_acct == "NGN":
                    if (float(amount)) > all_d.usd:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                        "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.usd

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.bal)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(bal=new)
                        Account.objects.filter(user_id=c_id).update(usd=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "USD/NGN/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "USD/NGN/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "USD", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "USD" and r_acct == "USD":
                    if (float(amount)) > all_d.usd:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.usd

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(amount) + r_bal.usd)
                        new2 = (s_bal - float(amount))

                        Account.objects.filter(user_id=r_info.id).update(usd=new1)
                        Account.objects.filter(user_id=c_id).update(usd=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "USD/USD/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "USD/USD/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "USD", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "USD" and r_acct == "EUR":
                    if (float(amount)) > all_d.usd:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/EUR&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.usd

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.eur)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(eur=new)
                        Account.objects.filter(user_id=c_id).update(usd=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "USD/EUR/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "USD/EUR/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "USD", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "USD" and r_acct == "GBP":
                    if (float(amount)) > all_d.usd:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/GBP&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.usd

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.gbp)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(eur=new)
                        Account.objects.filter(user_id=c_id).update(usd=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "USD/GBP/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "USD/GBP/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "USD", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "EUR" and r_acct == "NGN":
                    if (float(amount)) > all_d.eur:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.eur

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.bal)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(bal=new)
                        Account.objects.filter(user_id=c_id).update(eur=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "EUR/NGN/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "EUR/NGN/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "EUR", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "EUR" and r_acct == "USD":
                    if (float(amount)) > all_d.eur:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/USD&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.eur

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.usd)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(usd=new)
                        Account.objects.filter(user_id=c_id).update(eur=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "EUR/USD/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "EUR/USD/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "EUR", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "EUR" and r_acct == "EUR":
                    if (float(amount)) > all_d.eur:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.eur

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(amount) + r_bal.eur)
                        new2 = (s_bal - float(amount))

                        Account.objects.filter(user_id=r_info.id).update(eur=new1)
                        Account.objects.filter(user_id=c_id).update(eur=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "EUR/EUR/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "EUR/EUR/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "EUR", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "EUR" and r_acct == "GBP":
                    if (float(amount)) > all_d.eur:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/GBP&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.eur

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.gbp)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(gbp=new)
                        Account.objects.filter(user_id=c_id).update(eur=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "EUR/GBP/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "EUR/GBP/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "EUR", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "GBP" and r_acct == "NGN":
                    if (float(amount)) > all_d.gbp:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.gbp

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.bal)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(bal=new)
                        Account.objects.filter(user_id=c_id).update(gbp=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "GBP/NGN/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "GBP/NGN/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "GBP", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "GBP" and r_acct == "USD":
                    if (float(amount)) > all_d.gbp:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/USD&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.gbp

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.usd)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(usd=new)
                        Account.objects.filter(user_id=c_id).update(gbp=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "GBP/USD/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "GBP/USD/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "GBP", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "GBP" and r_acct == "EUR":
                    if (float(amount)) > all_d.gbp:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/EUR&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                        payload = {}
                        headers = {
                        'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                        }

                        response = requests.request("POST", url, headers=headers, data = payload)

                        #print(response.text.encode('utf8'))
                        r_dict = json.loads(response.text)
        
                        e = []
                        for i in r_dict:
                            dt = r_dict[i]
                            txn = e.append(dt)
                        trans = e[3]
                        total = trans['total']
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.gbp

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(total) + r_bal.eur)
                        new2 = (s_bal - float(amount))
                        new = (str(round(new1, 2)))
                        Account.objects.filter(user_id=r_info.id).update(eur=new)
                        Account.objects.filter(user_id=c_id).update(gbp=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "GBP/EUR/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                            }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "GBP/EUR/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "GBP", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                elif s_acct == "GBP" and r_acct == "GBP":
                    if (float(amount)) > all_d.gbp:
                        error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Insufficient Balance"
                            }
                        }
                        return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        # Sender Info
                        bal = Account.objects.all().get(user_id=c_id)
                        s_bal = bal.gbp

                        # Receiver Info
                        r_info = UserModel.objects.all().get(email=r_email)
                        r_bal = Account.objects.all().get(user_id=r_info.id)
        
                        new1 = (float(amount) + r_bal.gbp)
                        new2 = (s_bal - float(amount))

                        Account.objects.filter(user_id=r_info.id).update(gbp=new1)
                        Account.objects.filter(user_id=c_id).update(gbp=new2)

                        data = {
                            "user_id": c_id,
                            "payment_ref": txt_id,
                            "payment_date": now,
                            "rec_email": r_email,
                            "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "GBP/GBP/" + r_name,
                            "payment_method": "WALLET",
                            "trans_ref": trans_ref,
                            "amount": amount,
                            "cur": "NGN", 
                            "payment_status": "PAID"
                        }

                        resp = {
                            "status": {
                                "status": status.HTTP_200_OK
                            },
                            "data": {
                                "user_id": c_id,
                                "payment_ref": txt_id,
                                "payment_date": now,
                                "rec_email": r_email,
                                "payment_desc": "WLT|" + s_all.last_name + "|TO|" + "GBP/GBP/" + r_name,
                                "payment_method": "WALLET",
                                "trans_ref": trans_ref,
                                "amount": amount,
                                "cur": "GBP", 
                                "payment_status": "PAID"
                            }
                        }

                        tx = TransactionsSerializers(data=data)
                        if tx.is_valid():
                            tx.save()

                        # Sender Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                        html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        #Reciever Email Notification
                        subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                        html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                        text_content = strip_tags(html_content)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        return Response(data=resp, status=status.HTTP_200_OK)
                else:
                    error = {
                        "status": status.HTTP_400_BAD_REQUEST,
                        "data": {
                            "message": "Invalid Transaction"
                            }
                        }
                    return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
        else:
            error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                "message": "User doesn't exist"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def confirmBank(request):
    if request.method == "POST":
        acct_no = request.data.get('accountNumber')
        bankCode = request.data.get('bankCode')

        url = f"https://sandbox.monnify.com/api/v1/disbursements/account/validate?accountNumber={acct_no}&bankCode={bankCode}"

        payload = {}
        headers= {}

        response = requests.request("GET", url, headers=headers, data = payload)

        r_dict = json.loads(response.text)

        acc = []
        for i in r_dict:
            bnk = r_dict[i]
            acc.append(bnk)
        statu = acc[1]
        if statu == "success":
            ac_d = acc[3]

            data = {
                "status": {
                        "status": status.HTTP_200_OK
                },
                "accountNumber": ac_d['accountNumber'],
                "accountName": ac_d['accountName'],
                "bankCode": ac_d['bankCode']
            }
            return Response(data=data, status=status.HTTP_200_OK)
        else:
            error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                "message": "Invalid Account Details"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def bankTransfer(request):
    U = 14
    res1 = ''.join(random.choices(string.digits, k=U))
    txn1 = str(res1)
    N = 6
    res2 = ''.join(random.choices(string.digits, k=N))
    txn2 = str(res2)
    trans_ref = "WDRT|" + txn1 + "|" + txn2
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_id = request.user.id
    U = 6
    res = ''.join(random.choices(string.digits, k=U))
    txn = str(res)
    txt_id = "TX" + txn
    c_user_name = request.user.last_name
    r_email = request.user.email
    if request.method == "POST":
        amount = request.data.get('amount')
        acct_num = request.data.get('accountNumber')
        bnk_code = request.data.get('bankCode')
        bals = Account.objects.all().get(user_id=c_id)
        if float(amount) > bals.bal:
            error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                "message": "Insufficient Balance"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
        else:
            url = "https://sandbox.monnify.com/api/v1/disbursements/single"

            payload = '{\n    \"amount\": '+ amount +',\n    \"reference\":\"'+ txt_id +'\",\n    \"narration\":\"Withdraw\",\n    \"bankCode\": \"' + bnk_code +'\",\n    \"accountNumber\": \"'+ acct_num +'\",\n    \"currency\": \"NGN\",\n    \"walletId\": \"654CAB2118124760A659C787B2AA38E8\"\n}'
            headers = {
            'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy',
            'Content-Type': 'application/json'
            }

            response = requests.request("POST", url, headers=headers, data = payload)

            r_dict = json.loads(response.text)

            resp = []
            for i in r_dict:
                bnk = r_dict[i]
                resp.append(bnk)
            res = resp[3]
            amount = res['amount']
            ref = res['reference']
            statu = res['status']

            # Customer Info

            new = (bals.bal - float(amount))
            Account.objects.filter(user_id=c_id).update(bal=new)

            data = {
                "user_id": c_id,
                "payment_ref": ref,
                "payment_date": now,
                "rec_email": r_email,
                "payment_desc": "BANK TRANSFER",
                "payment_method": "TRANSFER",
                "trans_ref": trans_ref,
                "amount": amount,
                "cur": "NGN", 
                "payment_status": statu
                }

            res = {
                "status": {
                    "status": status.HTTP_200_OK
                },
                "data": {
                "user_id": c_id,
                "payment_ref": ref,
                "payment_date": now,
                "rec_email": r_email,
                "payment_desc": "BANK TRANSFER",
                "payment_method": "TRANSFER",
                "trans_ref": trans_ref,
                "amount": amount,
                "cur": "NGN", 
                "payment_status": statu
                }
            }

            tx = TransactionsSerializers(data=data)
            if tx.is_valid():
                tx.save()
            # Sender Email Notification
            subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
            html_content = render_to_string('mail/transfer.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': 'TRF/' + acct_num, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return Response(data=res, status=status.HTTP_200_OK)



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def mobileData(request):
    T = 6
    res = ''.join(random.choices(string.digits, k=T))
    txn = str(res)
    txt_id = "TX" + txn
    c_user_name = request.user.last_name
    c_user_id = request.user.id
    c_user_phone = request.user.phone
    c_email = request.user.email
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_email = request.user.email
    if request.method == "POST":
        d_pack = request.data.get('serviceID')
        sel_pack = request.data.get('variation_code')
        m_no = request.data.get('mobileNumber')
        amount = Utility.objects.values('variation_amount').get(variation_code=sel_pack)['variation_amount']
        c_bal = Account.objects.all().get(user_id=c_user_id)

        url = "https://sandbox.vtpass.com/api/pay"

        payload = '{\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ d_pack +'\",\n\t\"billersCode\" : \"'+ m_no +'\",\n\t\"variation_code\" : \"'+ sel_pack +'\",\n\t\"amount\" : \"'+ str(amount) +'\",\n\t\"phone\" : \"'+ m_no +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6InZwdE5UdmNoYVhSVjZYQlJlcUVIQmc9PSIsInZhbHVlIjoieDBHc1BNaytJb29OM1BBOGg0c0lzM1dEa3NRRmRSSlk2TjBDZlcwZDNoREhNQkJrK0Z1VEpuckUydlpBWkJ1c001MHJpVGVOVWhrTklwYkRra3BjOHc9PSIsIm1hYyI6IjdjNTQ2N2UwNDFiNmFmZDNiM2ZjZTlkYjBkOGVjY2YyYWM5MTcxNjhjMGQ2NTlkZDg2NzI0ODNjOWUzMWE1NDEifQ%3D%3D'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            dat = d_dict[j]
            data_ = f.append(dat)
        data_resp = f[1]
        #print(data_resp)
        p_descr = data_resp['transactions']['product_name']
        trans_type = data_resp['transactions']['type']
        txf = data_resp['transactions']['transactionId']
        statu = data_resp['transactions']['status']
        mno = data_resp['transactions']['unique_element']
        if statu == "delivered":
            new = (c_bal.bal - float(amount))
            Account.objects.filter(user_id=c_user_id).update(bal=new)


            data = {
                "user_id": c_user_id,
                "payment_ref": txt_id,
                "payment_date": now,
                "rec_email": c_email,
                "payment_desc": p_descr + "/" + mno + "/" + trans_type,
                "payment_method": "WALLET",
                "trans_ref": txf,
                "amount": amount,
                "cur": "NGN", 
                "payment_status": "PAID"
                }

            res = {
                "status": {
                    "status": status.HTTP_200_OK
                },
                "data": {
                "user_id": c_user_id,
                "payment_ref": txt_id,
                "payment_date": now,
                "rec_email": c_email,
                "payment_desc": p_descr + "/" + mno + "/" + trans_type,
                "payment_method": "WALLET",
                "trans_ref": txf,
                "amount": amount,
                "cur": "NGN", 
                "payment_status": "PAID"
                }
            }

            tx = TransactionsSerializers(data=data)
            if tx.is_valid():
                tx.save()
            
            subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
            html_content = render_to_string('mail/data.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': d_pack, 'trans_ref': txf})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return Response(data=res, status=status.HTTP_200_OK)
        else:
            error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": "Transaction fail"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def all_code(request):
    try:
        snippet = Utility.objects.filter()
    except Utility.DoesNotExist:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    serializer = UtilitySerializers(instance=snippet, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def Airtime(request):
    U = 14
    res1 = ''.join(random.choices(string.digits, k=U))
    txn1 = str(res1)
    N = 6
    res2 = ''.join(random.choices(string.digits, k=N))
    txn2 = str(res2)
    c_first_name = request.user.first_name
    c_user_id = request.user.id
    c_email = request.user.email
    trans_ref = "WLTP|" + txn1 + "|" + txn2
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    H = 6
    res = ''.join(random.choices(string.digits, k=H))
    txn = str(res)
    txt_id = "TX" + txn
    if request.method == "POST":
        amount = request.data.get('amount')
        mobile = request.data.get('mobileNumber')
        network = request.data.get('network')
        code = "+234"
        mb = code + mobile[1:]
        amt = float(amount)
        act_bal = Account.objects.values('bal').get(user_id=c_user_id)['bal']
        new_bal = act_bal - amt
        if amt > act_bal:
            error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": "Insufficient Balance"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
        else:
            url = "https://sandbox.vtpass.com/api/pay"

            payload = '{\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ network +'\",\n\t\"amount\" : \"'+ amount +'\",\n\t\"phone\" : \"'+ mobile +'\"\n}'
            headers = {
            'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
            'Content-Type': 'application/json',
            'Cookie': 'laravel_session=eyJpdiI6IkYrQUpSMXR2SndhNGphYVA5M0hhUXc9PSIsInZhbHVlIjoiUHdzdUlwUlBnWXh2RmN0Y3lDalIwV0dmY21Nd3JZMEFIMm9wRmFoRUVua044aUYranBBWmpCemlWSWxiN0RiZyswcWpxRDQ3allGMG0rOXBIYVpST2c9PSIsIm1hYyI6IjFhZjA5MWUwNTRmODY4YWQ2MDcxZjViYTMwNTc4MjE0OWJhMWYxZWM1NjIzNDNlZDZkOGFkMjAxZGQ3YTEwYmIifQ%3D%3D'
            }

            response = requests.request("POST", url, headers=headers, data = payload)
            #print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                airt = d_dict[j]
                airtime = f.append(airt)
            air_resp = f[1]
            #print(air_resp)
            statu = air_resp['transactions']['status']
            p_method = "WALLET"
            p_descr = air_resp['transactions']['product_name']
            mob = air_resp['transactions']['unique_element']
            typ = air_resp['transactions']['type']
            txf = air_resp['transactions']['transactionId']
            if statu == "delivered":
                Account.objects.filter(user_id=c_user_id).update(bal=new_bal)

                data = {
                "user_id": c_user_id,
                "payment_ref": txt_id,
                "payment_date": now,
                "rec_email": c_email,
                "payment_desc": p_descr + "/" + mob + "/" + typ,
                "payment_method": "WALLET",
                "trans_ref": txf,
                "amount": amount,
                "cur": "NGN", 
                "payment_status": "PAID"
                }

                res = {
                "status": {
                    "status": status.HTTP_200_OK
                },
                "data": {
                "user_id": c_user_id,
                "payment_ref": txt_id,
                "payment_date": now,
                "rec_email": c_email,
                "payment_desc": p_descr + "/" + mob + "/" + typ,
                "payment_method": "WALLET",
                "trans_ref": txf,
                "amount": amount,
                "cur": "NGN", 
                "payment_status": "PAID"
                    }
                }

            tx = TransactionsSerializers(data=data)
            if tx.is_valid():
                tx.save()

                subject, from_email, to = 'Payment Notification', EMAIL_FROM, c_email
                html_content = render_to_string('mail/trans.html',
                                            {'c_first_name': c_first_name, 'txn_id': txt_id, 'trans_ref': txf, 'amount': amount, 
                                            'p_method': p_method, 'p_descr': p_descr, 'date': now})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return Response(data=res, status=status.HTTP_200_OK)
            else:
                error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": "Fail Transaction"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def powerVerify(request):
    if request.method == "POST":
        plan = request.data.get('serviceType')
        sub = request.data.get('serviceID')
        m_no = request.data.get('meterNumber')

        url = "https://sandbox.vtpass.com/api/merchant-verify"

        payload = '{\n\t\n\t\"billersCode\" : '+ m_no +',\n\t\"serviceID\" : \"'+ sub +'\",\n\t\"type\": \"'+ plan +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6Im1vanQ3OTI5V2NpQWVSUG1nYTZIbWc9PSIsInZhbHVlIjoibWFTclwvUW5ibnFVb29iUTRFa0MxdjBwN1U0ZlduRjhmMmF3XC9XbWFZK21HN1VJQkM0YmRIWkZlQUh4bU9halhabjdnUzI5YWI3R3FjZEQ5TGtrQWd3UT09IiwibWFjIjoiNTQyODA4NzllODM4MTIwZTdkNzY0Nzg4ZTU5ZjI2Yzg3N2EwMzdjZWVmNGM4NDQ0MDdkNmFlMTlhMTI4NWI4MSJ9'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            tv = d_dict[j]
            s_tv = f.append(tv)
        p_resp = f[1]
        print(p_resp)
        if 'error' in p_resp:
            error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": p_resp['error']
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
        else:
            if int(m_no) or m_no in p_resp.values():
                cus_name = p_resp['Customer_Name']
                if sub == "ibadan-electric" or sub == "jos-electric" or sub == "portharcourt-electric":
                    cus_name = p_resp['Customer_Name']
                    MeterNumber = p_resp['MeterNumber']
                    address = p_resp['Address']
                    data = {
                        "status":{
                            "status": status.HTTP_200_OK
                        },
                        "customerName": cus_name,
                        "MeterNumber": MeterNumber,
                        "address": address,
                        "serviceType": plan,
                        "serviceID": sub
                    }
                    return Response(data=data, status=status.HTTP_200_OK)
                elif p_resp['Customer_Name'] in p_resp.values():
                    m_no = p_resp['Meter_Number'] 
                    cus_dist = p_resp['Customer_District']
                    address = p_resp['Address']
                    data = {
                        "status":{
                            "status": status.HTTP_200_OK
                        },
                        "customerName": cus_name,
                        "MeterNumber": m_no,
                        "address": address,
                        "serviceType": plan,
                        "serviceID": sub,
                        "customerDistrict": cus_dist
                    }
                    return Response(data=data, status=status.HTTP_200_OK)
                else:
                    error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": p_resp['error']
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def buyPower(request):
    T = 6
    res = ''.join(random.choices(string.digits, k=T))
    txn = str(res)
    txt_id = "TX" + txn

    c_user_id = request.user.id
    c_user_phone = request.user.phone
    c_user_name = request.user.last_name
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_email = request.user.email
    if request.method == "POST":
        plan = request.data.get('variation_code')
        sub = request.data.get('serviceID')
        amount = request.data.get('amount')
        m_no = request.data.get('meterNumber')
        c_bal = Account.objects.all().get(user_id=c_user_id)

        url = "https://sandbox.vtpass.com/api/pay"

        payload = '{\n\t\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ sub +'\",\n\t\"billersCode\" : \"'+ m_no +'\",\n\t\"variation_code\": \"'+ plan +'\",\n\t\"amount\" : \"'+ amount +'\",\n\t\"phone\": \"'+ c_user_phone +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6InZKWlwvMU5DYkxBV0NCNGI2dW1BXC9KQT09IiwidmFsdWUiOiJIbVRZTkkxYTh3ZXk4QzZnQkM2MCtZbVBtRkdha2tVYlVTNk9kWndoZXpVTlZmeGxRT3R2MkUzcWdERlFiQlBSeTBIWFhtb2dJdHB5NnhmdzdDUHp1dz09IiwibWFjIjoiMWEyNTVkNDk2YmQ4ZmU5M2MzOGE1YjJlZjZiM2Y4MDUxMGJmNWY4MDZkYjA4ZjExMjYxMDM1YTYzMzNkYmE3NiJ9'
        }

        response = requests.request("POST", url, headers=headers, data = payload)
        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            po = d_dict[j]
            s_po = f.append(po)
        po_resp = f[1]
        statu = po_resp['transactions']['status']
        desc = po_resp['transactions']['product_name']
        tran_id = po_resp['transactions']['transactionId']
        #print(po_resp)
        code = f[6]
        #print(plan)
        if statu == "delivered":
            if plan == "postpaid":
                new = (c_bal.bal - float(amount))
                Account.objects.filter(user_id=c_user_id).update(bal=new)

                data = {
                    "user_id": c_user_id,
                    "payment_ref": txt_id,
                    "payment_date": now,
                    "rec_email": c_email,
                    "payment_desc": desc,
                    "payment_method": "WALLET",
                    "trans_ref": tran_id,
                    "amount": amount,
                    "cur": "NGN", 
                    "payment_status": "PAID"
                }

                electric = {
                    "owner_id": c_user_id,
                    "txn_id": txt_id,
                    "txn_ref": tran_id,
                    "desc": desc,
                    "token": "POSTPAID",
                    "date_purchase": now
                }

                res = {
                    "status": {
                        "status": status.HTTP_200_OK
                    },
                    "data": {
                    "user_id": c_user_id,
                    "payment_ref": txt_id,
                    "payment_date": now,
                    "rec_email": c_email,
                    "payment_desc": desc,
                    "payment_method": "WALLET",
                    "trans_ref": tran_id,
                    "amount": amount,
                    "cur": "NGN", 
                    "payment_status": "PAID",
                    "Token": {
                        "Token": "postpaid",
                        "date": now
                    }
                    }
                }

                tx = TransactionsSerializers(data=data)
                if tx.is_valid():
                    tx.save()

                elect = ElectricitySerializers(data=electric)
                if elect.is_valid():
                    elect.save()

                #Buyer/User Email 
                subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
                html_content = render_to_string('mail/elect.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()

                #Sending of the Electricty Token

                subject, from_email, to = 'Electricity PIN/Token', EMAIL_FROM, c_email
                html_content = render_to_string('mail/elect2.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id, 'token': code})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return Response(data=res, status=status.HTTP_200_OK)
            else:
                new = (c_bal.bal - float(amount))
                Account.objects.filter(user_id=c_user_id).update(bal=new)

                data = {
                    "user_id": c_user_id,
                    "payment_ref": txt_id,
                    "payment_date": now,
                    "rec_email": c_email,
                    "payment_desc": desc,
                    "payment_method": "WALLET",
                    "trans_ref": tran_id,
                    "amount": amount,
                    "cur": "NGN", 
                    "payment_status": "PAID"
                }

                electric = {
                    "owner_id": c_user_id,
                    "txn_id": txt_id,
                    "txn_ref": tran_id,
                    "desc": desc,
                    "token": code,
                    "date_purchase": now
                }

                res = {
                    "status": {
                        "status": status.HTTP_200_OK
                    },
                    "data": {
                    "user_id": c_user_id,
                    "payment_ref": txt_id,
                    "payment_date": now,
                    "rec_email": c_email,
                    "payment_desc": desc,
                    "payment_method": "WALLET",
                    "trans_ref": tran_id,
                    "amount": amount,
                    "cur": "NGN", 
                    "payment_status": "PAID",
                    "Token": {
                        "Token": code,
                        "date": now
                    }
                    }
                }

                tx = TransactionsSerializers(data=data)
                if tx.is_valid():
                    tx.save()

                elect = ElectricitySerializers(data=electric)
                if elect.is_valid():
                    elect.save()

                #Buyer/User Email 
                subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
                html_content = render_to_string('mail/elect.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()

                #Sending of the Electricty Token

                subject, from_email, to = 'Electricity PIN/Token', EMAIL_FROM, c_email
                html_content = render_to_string('mail/elect2.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id, 'token': code})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return Response(data=res, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def allElectricity(request):
    c_id = request.user.id

    try:
        show = Electricity.objects.filter(owner_id=c_id)
    except Electricity.DoesNotExist:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    serializer = ElectricitySerializers(instance=show, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def tvVerify(request):
    U = 6
    res = ''.join(random.choices(string.digits, k=U))
    txn = str(res)
    txt_id = "TX" + txn
    c_email = request.user.email
    c_id = request.user.id
    c_user_id = request.user.id
    if request.method == "POST":
        sno = request.data.get('cardNumber')
        biller = request.data.get('biller')
        url = "https://sandbox.vtpass.com/api/merchant-verify"

        payload = '{\n\t\n\t\"billersCode\" : '+ sno +',\n\t\"serviceID\" : \"'+ biller +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6IkFQYW1NZExRblhrQWhnMDRMb2IwY3c9PSIsInZhbHVlIjoiTnVrMURUeFIwbTlLeUpMZ25jTTB0eGkzeDVaZ1ZnQkRWMzFJS0lFQ3JqTGx3NzRSMXNxczdHVGlGS0xJRkFLbVF1akhxMms3TndwZUtLc0x1K205d3c9PSIsIm1hYyI6IjZkYTA3NDliOGE0OGZkNWY2MTliNTNlM2EzOWQyYjYyYmMyOWEyNGI4ZDA5NWZmMmNhNjJjZWYxYjU5NWIxNDEifQ%3D%3D'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            tv = d_dict[j]
            s_tv = f.append(tv)
        tv_resp = f[1]
        #print(tv_resp)
        status_code = f[0]
        bill = "startimes"
        if int(sno) or sno in tv_resp.values():
            cus_name = tv_resp['Customer_Name']
            #print(sno in tv_resp.values())
            if biller == bill:
                cus_id = tv_resp['Smartcard_Number']
                show = Utility.objects.filter(service_id=biller)

                data = {
                    "status": {
                        "status_code": status.HTTP_200_OK
                    },
                    "customerName": cus_name,
                    "cardNumber": sno,
                    "customerID": cus_id,
                }
                return Response(data=data, status=status.HTTP_200_OK)
            elif cus_name in tv_resp.values():
                cus_id = tv_resp['Customer_ID']
                data = {
                    "status": {
                        "status_code": status.HTTP_200_OK
                    },
                    "customerName": cus_name,
                    "cardNumber": sno,
                    "customerID": cus_id,
                }
                return Response(data=data, status=status.HTTP_200_OK)
            else:
                error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": "Error"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
        else:
            error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": "Invalid Card Number"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def payTv(request):
    T = 6
    res = ''.join(random.choices(string.digits, k=T))
    txn = str(res)
    txt_id = "TX" + txn

    c_user_id = request.user.id
    c_user_phone = request.user.phone
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_email = request.user.email
    c_user_name = request.user.last_name
    if request.method == "POST":
        variation_code = request.data.get('variation_code')
        sno = request.data.get('cardNumber')
        amount = Utility.objects.values('variation_amount').get(variation_code=variation_code)['variation_amount']
        service_id = Utility.objects.values('service_id').get(variation_code=variation_code)['service_id']
        amt = float(amount)
        act_bal = Account.objects.values('bal').get(user_id=c_user_id)['bal']
        new_bal = act_bal - amt
        if amt > act_bal:
            error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": "Insufficient Balance"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
        else:
            url = "https://sandbox.vtpass.com/api/pay"

            payload = '{\n\t\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ service_id +'\",\n\t\"billersCode\" : \"'+ sno +'\",\n\t\"variation_code\": \"'+ variation_code +'\",\n\t\"amount\" : \"'+ str(amount) +'\",\n\t\"phone\": \"'+ c_user_phone +'\"\n}'
            headers = {
            'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
            'Content-Type': 'application/json',
            'Cookie': 'laravel_session=eyJpdiI6ImJOMSs1MHpYejlYeEQyV01TSEVSRWc9PSIsInZhbHVlIjoidE9KaDc4MFFQYVkyS3duTVBHQXVsVHpsRlcwbHV0SG5FZ2pJOHVtY3RMd3BJWGM4QW5RdU5rQ1hzSzQ5UFVodzRhUVE0RGFVeEFxK0dPUktqV1BSa0E9PSIsIm1hYyI6ImFhZGMzMDY0OGIyYmViM2QwNDAyMDYxOWMwYWIwOGM2MmYxYzZhN2IzYzE4YzcyOTkzY2JjOTU5NTIzNDViZGUifQ%3D%3D'
            }

            response = requests.request("POST", url, headers=headers, data = payload)

            #print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                airt = d_dict[j]
                airtime = f.append(airt)
            tv_resp = f[1]['transactions']
            statu = tv_resp['status']
            p_name = tv_resp['product_name']
            cno = tv_resp['unique_element']
            desc = tv_resp['type']
            trans_id = tv_resp['transactionId']
            if statu == "delivered":
                Account.objects.filter(user_id=c_user_id).update(bal=new_bal)

                data = {
                    "user_id": c_user_id,
                    "payment_ref": txt_id,
                    "payment_date": now,
                    "rec_email": c_email,
                    "payment_desc": desc + "/" + p_name + "/" + cno,
                    "payment_method": "WALLET",
                    "trans_ref": trans_id,
                    "amount": amount,
                    "cur": "NGN", 
                    "payment_status": "PAID"
                }

                res = {
                    "status": {
                        "status": status.HTTP_200_OK
                    },
                    "data": {
                    "user_id": c_user_id,
                    "payment_ref": txt_id,
                    "payment_date": now,
                    "rec_email": c_email,
                    "payment_desc": desc + "/" + p_name + "/" + cno,
                    "payment_method": "WALLET",
                    "trans_ref": trans_id,
                    "amount": amount,
                    "cur": "NGN", 
                    "payment_status": "PAID"
                    }
                }

                tx = TransactionsSerializers(data=data)
                if tx.is_valid():
                    tx.save() 
                
                # Sender Email Notification
                subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
                html_content = render_to_string('mail/dstv.html',
                                        {'last_name': c_user_name, 'date': now, 'reciever': desc + '/' + cno + '/' + p_name, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_id})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return Response(data=res, status=status.HTTP_200_OK)
            else:
                error = {
                "status": status.HTTP_400_BAD_REQUEST,
                "data": {
                    "message": "Fail"
                }
            }
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)