from djoser.serializers import UserCreateSerializer, UserSerializer
from rest_framework import serializers
from .models import *
from user.models import *


class QuerySerializers(serializers.ModelSerializer):
    class Meta:
        model = Transactions
        fields = "__all__"


class TransactionsSerializers(serializers.ModelSerializer):
    class Meta:
        model = Transactions
        fields = "__all__"


class UtilitySerializers(serializers.ModelSerializer):
    class Meta:
        model = Utility
        fields = "__all__"


class ElectricitySerializers(serializers.ModelSerializer):
    class Meta:
        model = Electricity
        fields = "__all__"
