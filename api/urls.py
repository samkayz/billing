from django.urls import path, include
from api import views


urlpatterns = [
    path('', include('djoser.urls')),
    path('', include('djoser.urls.authtoken')),
    path('query/<ref>', views.query, name='query'),
    path('verify', views.verify, name='verify'),
    path('send_money', views.send_money, name='send_money'),
    path('confirmBank', views.confirmBank, name='confirmBank'),
    path('bankTransfer', views.bankTransfer, name='bankTransfer'),
    path('mobileData', views.mobileData, name='mobileData'),
    path('all_code', views.all_code, name='all_code'),
    path('Airtime', views.Airtime, name='Airtime'),
    path('accountNo', views.accountNo, name='accountNo'),
    path('powerVerify', views.powerVerify, name='powerVerify'),
    path('buyPower', views.buyPower, name='buyPower'),
    path('allElectricity', views.allElectricity, name='allElectricity'),
    path('tvVerify', views.tvVerify, name='tvVerify'),
    path('payTv', views.payTv, name='payTv'),
]