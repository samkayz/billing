from django.db import models


class Commission(models.Model):
    total_charge = models.FloatField(default="0")

    class Meta:
        db_table ='commission'

