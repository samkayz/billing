from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from django.contrib.auth.decorators import login_required, permission_required
from user.models import *
from .models import *
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.db.models import Sum
from billing.settings import EMAIL_FROM
import uuid
import re
import csv
import io
UserModel = get_user_model()


def login(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        if UserModel.objects.filter(is_superuser=1).exists():
            users = authenticate(email=email, password=password)

            if users is not None:
                dj_login(request, users)
                return redirect('home')
            else:
                messages.error(request, 'Access Denied!!')
                return redirect('/web/login')
        else:
            messages.error(request, 'User not found')
            return redirect('/web/login')
    else:
        return render(request, 'admin/index.html')


@permission_required('is_superuser', login_url='/web/login')
def home(request):
    show = Transactions.objects.aggregate(Sum('amount'))['amount__sum']
    all_trans = Transactions.objects.filter().order_by('-id')[:8]
    return render(request, 'admin/home.html', {'all_trans': all_trans, 'show': show})


def logout(request):
    s_logout(request)
    return redirect('/web/login')


@permission_required('is_superuser', login_url='/web/login')
def users(request):
    allUser = UserModel.objects.filter().order_by('-id')
    return render(request, 'admin/users.html', {'allUser': allUser})


@permission_required('is_superuser', login_url='/web/login')
def view_user(request, id):
    view_ = UserModel.objects.all().get(id=id)
    bal = Account.objects.all().get(user_id=id)
    return render(request, 'admin/view_user.html', {'view_': view_, 'bal': bal})


@permission_required('is_superuser', login_url='/web/login')
def block_user(request, id):
    UserModel.objects.filter(id=id).update(is_active=0)
    return redirect(f'/web/view_user/{id}')


@permission_required('is_superuser', login_url='/web/login')
def unblock_user(request, id):
    UserModel.objects.filter(id=id).update(is_active=1)
    return redirect(f'/web/view_user/{id}')


@permission_required('is_superuser', login_url='/web/login')
def user_trans(request, id):
    info = UserModel.objects.all().get(id=id)
    all_trans = Transactions.objects.filter(Q(user_id=id) | Q(rec_email=info.email)).order_by('-id')
    return render(request, 'admin/user_trans.html', {'all_trans': all_trans, 'info': info})


@permission_required('is_superuser', login_url='/web/login')
def electricity(request):
    show = Electricity.objects.filter().order_by('-id')
    return render(request, 'admin/electricity.html', {'show': show})


@permission_required('is_superuser', login_url='/web/login')
def application(request):
    allUser = ReceiveMoneyForm.objects.filter().order_by('-id')
    return render(request, 'admin/application.html', {'allUser': allUser})


@permission_required('is_superuser', login_url='/web/login')
def view_app(request, id):
    all_info = ReceiveMoneyForm.objects.all().get(app_id=id)
    return render(request, 'admin/view_app.html', {'all_info': all_info})


@permission_required('is_superuser', login_url='/web/login')
def all_trans(request):
    all_trans = Transactions.objects.filter().order_by('-id')
    return render(request, 'admin/all_trans.html', {'all_trans': all_trans})


@permission_required('is_superuser', login_url='/web/login')
def all_invoice(request):
    all_invoice = Invoice.objects.filter().order_by('-id')
    return render(request, 'admin/all_invoice.html', {'all_invoice': all_invoice})


@permission_required('is_superuser', login_url='/web/login')
def invoice_detail(request, id):
    detail = Invoice.objects.all().get(trans_ref=id)
    own_id = Invoice.objects.values('user_id').get(trans_ref=id)['user_id']
    inv_owner = UserModel.objects.all().get(id=own_id)
    return render(request, 'admin/view_invoice.html', {'detail': detail, 'inv_owner': inv_owner})


@permission_required('is_superuser', login_url='/web/login')
def dstv_trans(request):
    dstv = Transactions.objects.filter(payment_desc="Dstv Payment").order_by('-id')
    return render(request, 'admin/dstv_trans.html', {'dstv': dstv})


@permission_required('is_superuser', login_url='/web/login')
def airtime_trans(request):
    airtime = Transactions.objects.filter(payment_desc="Airtime Purchase").order_by('-id')
    return render(request, 'admin/airtime_trans.html', {'airtime': airtime})


@permission_required('is_superuser', login_url='/web/login')
def all_withdraw(request):
    all_withdraw = Transactions.objects.filter(payment_desc="CASH WITHDRAW").order_by('-id')
    return render(request, 'admin/all_withdraw.html', {'all_withdraw': all_withdraw})


@permission_required('is_superuser', login_url='/web/login')
def dstv_upload(request):
    if request.method == 'POST':
        csv_file = request.FILES['file']
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File not csv')
            return redirect('dstv_upload')
        else:
            decoded_file = csv_file.read().decode('utf-8')
            io_string = io.StringIO(decoded_file)
            # next(io_string)
            for data in csv.reader(io_string, delimiter=',', quotechar='|'):
                VirtualPin.objects.update_or_create(pin=data[0], pin_amount=data[1], pin_status='0')
            messages.success(request, 'Record Updated')
            return redirect('dstv_upload')
            # print(data[1])
    else:
        return render(request, 'admin/dstv_upload.html')


@permission_required('is_superuser', login_url='/web/login')
def all_voucher(request):
    all_v= VirtualPin.objects.filter().order_by('-id')
    return render(request, 'admin/all_voucher.html', {'all_v': all_v})


@permission_required('is_superuser', login_url='/web/login')
def accounts(request):
    all_acct = ReservedAccount.objects.filter().order_by('-id')
    return render(request, 'admin/account.html', {'all_acct': all_acct})