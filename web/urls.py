from django.urls import path
from . import views


urlpatterns = [
    path('', views.login, name='login'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('home', views.home, name='home'),
    path('users', views.users, name='users'),
    path('view_user/<id>', views.view_user, name='view_user'),
    path('block_user/<id>', views.block_user, name='block_user'),
    path('unblock_user/<id>', views.unblock_user, name='unblock_user'),
    path('user_trans/<id>', views.user_trans, name='user_trans'),
    path('application', views.application, name='application'),
    path('electricity', views.electricity, name='electricity'),
    path('view_app/<id>', views.view_app, name='view_app'),
    path('all_trans', views.all_trans, name='all_trans'),
    path('all_invoice', views.all_invoice, name='all_invoice'),
    path('invoice_detail/<id>', views.invoice_detail, name='invoice_detail'),
    path('dstv_trans', views.dstv_trans, name='dstv_trans'),
    path('airtime_trans', views.airtime_trans, name='airtime_trans'),
    path('all_withdraw', views.all_withdraw, name='all_withdraw'),
    path('dstv_upload', views.dstv_upload, name='dstv_upload'),
    path('all_voucher', views.all_voucher, name='all_voucher'),
    path('accounts', views.accounts, name='accounts'),
]