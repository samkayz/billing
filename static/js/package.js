// this script jus generates dynamic html, nothing more

var Package = {
    'Dstv': [
       'DSTV Access', 'Desc', 'DSTV Yanga', 'DSTV Family', 'DSTV Confam','DSTV Compact', 'DSTV Compact Plus', 'DSTV Premium',
    ],
    'GOtv': [
       'GOtv Max', 'GOtv Jolli', 'GOtv Plus', 'GOtv Jinja', 'GOtv Value','GOtv Lite (Monthly)', 'GOtv Lite (Quarterly)',
       'GOtv Lite (Annual)',
    ],

};

var Price = {
    'DSTV Access': [ '2000'], 'DSTV Yanga': ['2500'], 'DSTV Family': ['4000'], 'DSTV Confam': ['4500'],
    'DSTV Compact' : [6800],'DSTV Compact Plus': [10650], 'DSTV Premium': [15800],
    'GOtv Max' : [3200], 'GOtv Jolli': [2,400], 'GOtv Plus' : [1900], 'GOtv Jinja' : [1600],
    'GOtv Value' : [1250], 'GOtv Lite (Monthly)' : [400], 'GOtv Lite (Quarterly)' : [1050],
    'GOtv Lite (Annual)' : [3100],
};


function usePackage(param){
    var parentE = document.getElementById('package');
    // console.log(param);
    console.log(parentE);
    var emptOptn = '<option value="" id="optG">Select Your Package</option>';
    parentE.innerHTML = emptOptn;
    for (var i = 0; i < param.length; i++) {
        var elem = '<option value="' + param[i] + '">' + param[i] + '</option>';
                    $(elem).insertBefore('#optG');
                // console.log(parentE);
    }
    console.log(document.getElementById('package'));
}


function useSelectedItem(param){
    console.log(param);
    var parentE = document.getElementById('package');
    console.log(parentE);
    // console.log(param.value);
    parentE.innerHTML = '';
    usePackage(Package[param.value]);
}

function usePrice(param){
    var parentE = document.getElementById('price');
    // console.log(param);
    console.log(parentE);
    var emptOptn = '<option value="" id="optsG">Select Amount</option>';
    parentE.innerHTML = emptOptn;
    for (var i = 0; i < param.length; i++) {
        var elem = '<option value="' + param[i] + '">' + param[i] + '</option>';
                    $(elem).insertBefore('#optsG');
                // console.log(parentE);
    }
    console.log(document.getElementById('price'));
}


function useAmountItem(param){
    console.log(param);
    var parentE = document.getElementById('price');
    console.log(parentE);
    // console.log(param.value);
    parentE.innerHTML = '';
    usePrice(Price[param.value]);
}


var Data = {

    'Airtel': [
        'Airtel Daily Boundles', 'Airtel Weekly Boundles', 'Airtel Monthly Boundles', 'Airtel Mega Boundles','Airtel Binge Plans', 
        'Airtel Social Boundles', 'Airtel Router Boundles'],
    'Etisalat': [
        'Etisalat Daily Boundles', 'Etisalat Weekly Boundles', 'Etisalat Monthly Boundles', 'Etisalat Mega Boundles',
        'Etisalat Binge Plans', 'Etisalat Social Boundles', 'Etisalat Router Boundles'],
    'Glo': [
        'Glo Daily Plans', 'Glo Weekly Plans', 'Glo Monthly Plans', 'Glo 2-Monthly Plans','Glo 3-Month Plans'
    ],
    'Mtn': ['Mtn Daily Plans', 'Mtn Weekly Plans', 'Mtn Monthly Plans', 'Mtn 2-Month Plans','Mtn 3-Month Plans'],

};

var DataPrice = {

    'Mtn Daily Plans': ['25NGN for WhatsApp Daily', '50NGN for 25MB', '100NGN for 75MB', '200NGN for 200MB (2- day plan)', '300NGN for 1GB','500NGN for 2GB ( 2-day Plan)'],
    'Mtn Weekly Plans': ['50NGN for 200MB (Web & App)', '300NGN for 350MB', '150NGN 700MB (Web & App)', '500 for 750MB (2-Week plan)'],
    'Mtn Monthly Plans': ['1000NGN for 1.5GB', '1200 for 2GB', '1500 for 3GB', '2000 for 4.5GB', '2500 for 6.5GB','3500 for 10GB', '5000 for 15GB'],
    'Mtn 2-Month Plans': ['20000 for 75GB', '30000 for 120GB'],
    'Mtn 3-Month Plans': ['50000 for 150GB', '75000 for 250GB'],


    'Airtel Daily Boundles': ['50NGN for 25MB Daily', '100NGN for 75MB Daily', '200NGN for 200MB (3 Days)', '300NGN for 350MB (7-day plan)', '500NGN for 750 (14 days)',
    '300NGN for 1GB ( A-day Plan)', '500 for 2GB (Daily)'],
    'Airtel Weekly Boundles': ['50NGN for 200MB (Web & App)', '300NGN for 350MB', '150NGN 700MB (Web & App)', '500 for 750MB (2-Week plan)'],
    'Airtel Monthly Boundles': ['1000NGN for 1.5GB', '1200 for 2GB', '1500 for 3GB', '2000 for 4.5GB', '2500 for 6.5GB','3500 for 10GB', '5000 for 15GB'],
    'Airtel Mega Boundles': ['20000 for 75GB', '30000 for 120GB'],
    'Airtel Social Boundles': ['50000 for 150GB', '75000 for 250GB'],
    'Airtel Router Boundles': ['50000 for 150GB', '75000 for 250GB'],

};

function useProvider(param){
    var parentE = document.getElementById('list');
    // console.log(param);
    console.log(parentE);
    var emptOptn = '<option value="" id="optG">Select Your Package</option>';
    parentE.innerHTML = emptOptn;
    for (var i = 0; i < param.length; i++) {
        var elem = '<option value="' + param[i] + '">' + param[i] + '</option>';
                    $(elem).insertBefore('#optG');
                // console.log(parentE);
    }
    console.log(document.getElementById('list'));
}

function useSelectedList(param){
    console.log(param);
    var parentE = document.getElementById('list');
    console.log(parentE);
    // console.log(param.value);
    parentE.innerHTML = '';
    useProvider(Data[param.value]);
}

function usePriceData(param){
    var parentE = document.getElementById('amt');
    // console.log(param);
    console.log(parentE);
    var emptOptn = '<option value="" id="amts">Select Amt</option>';
    parentE.innerHTML = emptOptn;
    for (var i = 0; i < param.length; i++) {
        var elem = '<option value="' + param[i] + '">' + param[i] + '</option>';
                    $(elem).insertBefore('#amts');
                // console.log(parentE);
    }
    console.log(document.getElementById('amt'));
}

function usePriceItem(param){
    console.log(param);
    var parentE = document.getElementById('amt');
    console.log(parentE);
    // console.log(param.value);
    parentE.innerHTML = '';
    usePriceData(DataPrice[param.value]);
}



