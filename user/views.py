from django.db.models import Q
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from django.contrib.auth import user_logged_in
from django.dispatch.dispatcher import receiver
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.paginator import Paginator
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.utils.html import strip_tags
from django.db.models import Sum
from billing.settings import EMAIL_FROM
from .models import *
import random
import string
import uuid
import datetime
import json
import math
import requests
from django.http import HttpResponse
from datetime import datetime
from django.views.decorators.http import require_http_methods
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
UserModel = get_user_model()
from django.core import serializers


def signin(request):
    if request.user.is_authenticated:
        return redirect('dashboard')
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        user = authenticate(email=email, password=password)

        if user is not None:
            dj_login(request, user)
            request.session.set_expiry(1200)
            return redirect('dashboard')
        else:
            messages.error(request, 'Invalid Credentials')
            return redirect('signin')
    else:
        return render(request, 'login.html')


def signup(request):
    N = 6
    res = ''.join(random.choices(string.digits, k=N))
    cus_id = str(res)
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M"))
    if request.user.is_authenticated:
        return redirect('dashboard')
    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        phone_no = request.POST['phone_no']
        email = request.POST['email']
        password = request.POST['password']
        acct_tp = request.POST['acct_type']
        bus_name = request.POST['bus_name']

        if UserModel.objects.filter(email=email).exists():
            messages.error(request, 'Email Taken')
            return redirect('signup')
        elif UserModel.objects.filter(phone=phone_no).exists():
            messages.error(request, 'Mobile Number Used')
            return redirect('signup')
        else:
            if acct_tp == "1":
                url = "https://sandbox.monnify.com/api/v1/auth/login"

                payload = {}
                headers = {
                'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy'
                }

                response = requests.request("POST", url, headers=headers, data = payload)

                response_dict = json.loads(response.text)
                h = []
                for i in response_dict:
                    data = response_dict[i]
                    h.append(data)
                e = []
                for r in h[3]:
                    toke = h[3][r]
                    e.append(toke)
                a = "Bearer"+ " " + e[0]

                url = "https://sandbox.monnify.com/api/v1/bank-transfer/reserved-accounts"

                payload = '{"accountReference\": \"'+ phone_no +'\",   \"accountName\": \"'+ first_name + " " + last_name +'\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"2917634474\",\n    \"customerEmail\": \"'+ email +'\"\n}'
                headers = {
                'Content-Type': 'application/json',
                'Authorization': a
                }

                response = requests.request("POST", url, headers=headers, data = payload)
                r_dict = json.loads(response.text)
                resv = []
                for j in r_dict:
                    data = r_dict[j]
                    resv.append(data)
                res = resv[3]
                print(res['contractCode'])
                print(res)
                acct_no = res['accountNumber']
                bank_name = res['bankName']
                bank_code = res['bankCode']
                reserve_ref = res['reservationReference']
                acct_type = res['reservedAccountType']
                acct_ref = res['accountReference']
                acct_name = res['accountName']
                c_email = res['customerEmail']
                status = res['status']
                cur = res['currencyCode']
                user = UserModel.objects.create_user(username=email,
                                                     password=password,
                                                     email=email,
                                                     first_name=first_name,
                                                     last_name=last_name,
                                                     phone=phone_no,
                                                     acct_type=acct_tp,
                                                     acct_no=acct_no,
                                                     acct_name=acct_name,
                                                     bank_name=bank_name,
                                                     bank_code=bank_code,
                                                     date_registered=now
                                                     )
                user.save()
                user_id = UserModel.objects.values('id').get(phone=phone_no)['id']
                acct_save = ReservedAccount(acct_no=acct_no, acct_name=acct_name, bank_name=bank_name, bank_code=bank_code,
                cur=cur, reserve_ref=reserve_ref, acct_type=acct_type, acct_ref=acct_ref, c_email=c_email, status=status)
                acct_save.save()
                user_acct = Account(user_id=user_id, bal='0', usd='0', eur='0', gbp='0')
                user_acct.save()
                subject, from_email, to = 'Welcome ' + last_name, EMAIL_FROM, email
                html_content = render_to_string('mail/welcome.html',
                                            {'first_name': first_name, 'last_name': last_name, 'acct_type': acct_type, 'acct_name': acct_name, 
                                            'acct_no': acct_no, 'bank_name': bank_name, 'date': now, 'bank_code': bank_code})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                # messages.info(request, 'Account Created Successfully, Kindly login with Your Username and Password')
                user = authenticate(email=email, password=password)
                dj_login(request, user)
                return redirect('dashboard')
            elif acct_tp == "2":
                if bus_name == "":
                    messages.error(request, "Business Name can't be empty")
                    return redirect('signup')
                else:
                    url = "https://sandbox.monnify.com/api/v1/auth/login"

                    payload = {}
                    headers = {
                    'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    response_dict = json.loads(response.text)
                    h = []
                    for i in response_dict:
                        data = response_dict[i]
                        h.append(data)
                    e = []
                    for r in h[3]:
                        toke = h[3][r]
                        e.append(toke)
                    a = "Bearer"+ " " + e[0]

                    url = "https://sandbox.monnify.com/api/v1/bank-transfer/reserved-accounts"

                    payload = '{"accountReference\": \"'+ phone_no +'\",   \"accountName\": \"'+ bus_name +'\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"2917634474\",\n    \"customerEmail\": \"'+ email +'\"\n}'
                    headers = {
                    'Content-Type': 'application/json',
                    'Authorization': a
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)
                    r_dict = json.loads(response.text)
                    resv = []
                    for j in r_dict:
                        data = r_dict[j]
                        resv.append(data)
                    res = resv[3]
                    print(res['contractCode'])
                    print(res)
                    acct_no = res['accountNumber']
                    bank_name = res['bankName']
                    bank_code = res['bankCode']
                    reserve_ref = res['reservationReference']
                    acct_type = res['reservedAccountType']
                    acct_ref = res['accountReference']
                    acct_name = res['accountName']
                    c_email = res['customerEmail']
                    status = res['status']
                    cur = res['currencyCode']
                    user = UserModel.objects.create_user(username=email,
                                                     password=password,
                                                     email=email,
                                                     first_name=first_name,
                                                     last_name=last_name,
                                                     phone=phone_no,
                                                     acct_type=acct_tp,
                                                     acct_no=acct_no,
                                                     acct_name=acct_name,
                                                     bank_name=bank_name,
                                                     bank_code=bank_code,
                                                     date_registered=now
                                                     )
                    user.save()
                    user_id = UserModel.objects.values('id').get(phone=phone_no)['id']
                    acct_save = ReservedAccount(acct_no=acct_no, acct_name=acct_name, bank_name=bank_name, bank_code=bank_code,
                    cur=cur, reserve_ref=reserve_ref, acct_type=acct_type, acct_ref=acct_ref, c_email=c_email, status=status)
                    acct_save.save()
                    user_acct = Account(user_id=user_id, bal='0', usd='0', eur='0', gbp='0')
                    user_acct.save()
                    subject, from_email, to = 'Welcome ' + last_name, EMAIL_FROM, email
                    html_content = render_to_string('mail/welcome.html',
                                            {'first_name': first_name, 'last_name': last_name, 'acct_type': acct_type, 'acct_name': acct_name, 
                                            'acct_no': acct_no, 'bank_name': bank_name, 'date': now, 'bank_code': bank_code})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    # messages.info(request, 'Account Created Successfully, Kindly login with Your Username and Password')
                    user = authenticate(email=email, password=password)
                    dj_login(request, user)
                    return redirect('dashboard')
            else:
                messages.error(request, "You have to choose account type")
                return redirect('signup')

    else:
        return render(request, 'signup.html')


@receiver(user_logged_in)
def remove_other_sessions(sender, user, request, **kwargs):
    # remove other sessions
    Session.objects.filter(usersession__user=user).delete()

    # save current session
    request.session.save()

    # create a link from the user to the current session (for later removal)
    UserSession.objects.get_or_create(
        user=user,
        session=Session.objects.get(pk=request.session.session_key)
    )


# Logout function the terminate all the login session
def logout(request):
    s_logout(request)
    return redirect('/')


@login_required(login_url='signin')
def dashboard(request):
    c_email = request.user.email
    c_id = request.user.id
    inv = Invoice.objects.filter(user_id=c_id).order_by('-id')[:4]
    bal = Account.objects.all().get(user_id=c_id)
    last_withdraw = Transactions.objects.filter(user_id=c_id, payment_desc="CASH WITHDRAW").order_by('-id')[:1]
    recent_withdraw = Transactions.objects.filter(user_id=c_id, payment_desc="CASH WITHDRAW").order_by('-id')[:3]
    show = Transactions.objects.filter(Q(user_id=c_id) | Q(rec_email=c_email)).order_by('-id')[:5]
    total_trans = Transactions.objects.filter(user_id=c_id).count()
    total_withdraw = Transactions.objects.filter(user_id=c_id, payment_desc="CASH WITHDRAW").aggregate(Sum('amount'))['amount__sum']
    return render(request, 'dashboard.html', {'bal': bal, 'show': show, 'inv': inv, 'last_withdraw': last_withdraw, 
    'total_trans': total_trans, 'recent_withdraw': recent_withdraw, 'total_withdraw': total_withdraw})


@login_required(login_url='signin')
def setting(request):
    c_id = request.user.id
    bal = Account.objects.all().get(user_id=c_id)
    bank = Banks.objects.filter()
    if BankAccount.objects.filter(user_id=c_id).exists():
        bnk_act = BankAccount.objects.all().get(user_id=c_id)
        return render(request, 'account.html', {'bnk_act': bnk_act, 'bank': bank})
    else:
        return render(request, 'account.html', {'bank': bank})


@login_required(login_url='signin')
def add_account(request):
    if request.method == "POST":
        user_id = request.user.id
        acct_no = request.POST['acct_no']
        ubank = request.POST['ubank']
        bank_name = Banks.objects.values('name').get(code=ubank)['name']
        if BankAccount.objects.filter(user_id=user_id).exists():
            BankAccount.objects.filter(user_id=user_id).update(account_no=acct_no, bank_code=ubank, bank_name=bank_name)
            return HttpResponse()
        else:
            act_bnk = BankAccount(user_id=user_id, account_no=acct_no, bank_code=ubank, bank_name=bank_name )
            act_bnk.save()
            return HttpResponse()


@login_required(login_url='signin')
def airtime_order(request):
    U = 14
    res1 = ''.join(random.choices(string.digits, k=U))
    txn1 = str(res1)
    N = 6
    res2 = ''.join(random.choices(string.digits, k=N))
    txn2 = str(res2)
    c_first_name = request.user.first_name
    c_user_id = request.user.id
    c_email = request.user.email
    trans_ref = "WLTP|" + txn1 + "|" + txn2
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    H = 6
    res = ''.join(random.choices(string.digits, k=H))
    txn = str(res)
    txt_id = "TX" + txn
    if request.method == "POST":
        amount = request.POST['amount']
        mobile = request.POST['mobile']
        network = request.POST['network']
        code = "+234"
        mb = code + mobile[1:]
        amt = float(amount)
        act_bal = Account.objects.values('bal').get(user_id=c_user_id)['bal']
        new_bal = act_bal - amt
        if amt > act_bal:
            response = HttpResponse(status=401)
            return response
        else:
            url = "https://sandbox.vtpass.com/api/pay"

            payload = '{\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ network +'\",\n\t\"amount\" : \"'+ amount +'\",\n\t\"phone\" : \"'+ mobile +'\"\n}'
            headers = {
            'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
            'Content-Type': 'application/json',
            'Cookie': 'laravel_session=eyJpdiI6IkYrQUpSMXR2SndhNGphYVA5M0hhUXc9PSIsInZhbHVlIjoiUHdzdUlwUlBnWXh2RmN0Y3lDalIwV0dmY21Nd3JZMEFIMm9wRmFoRUVua044aUYranBBWmpCemlWSWxiN0RiZyswcWpxRDQ3allGMG0rOXBIYVpST2c9PSIsIm1hYyI6IjFhZjA5MWUwNTRmODY4YWQ2MDcxZjViYTMwNTc4MjE0OWJhMWYxZWM1NjIzNDNlZDZkOGFkMjAxZGQ3YTEwYmIifQ%3D%3D'
            }

            response = requests.request("POST", url, headers=headers, data = payload)
            #print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                airt = d_dict[j]
                airtime = f.append(airt)
            air_resp = f[1]
            #print(air_resp)
            status = air_resp['transactions']['status']
            p_method = "WALLET"
            p_descr = air_resp['transactions']['product_name']
            mob = air_resp['transactions']['unique_element']
            typ = air_resp['transactions']['type']
            txf = air_resp['transactions']['transactionId']
            if status == "delivered":
                Account.objects.filter(user_id=c_user_id).update(bal=new_bal)
                tx = Transactions(user_id=c_user_id, payment_ref=txt_id, payment_date=now, rec_email=c_email, 
                payment_desc=p_descr + "/" + mob + "/" + typ, payment_method="WALLET", trans_ref=txf, cur="NGN", amount=amount, payment_status="PAID")
                tx.save()
                subject, from_email, to = 'Payment Notification', EMAIL_FROM, c_email
                html_content = render_to_string('mail/trans.html',
                                            {'c_first_name': c_first_name, 'txn_id': txt_id, 'trans_ref': txf, 'amount': amount, 
                                            'p_method': p_method, 'p_descr': p_descr, 'date': now})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return HttpResponse()
            else:
                response = HttpResponse(status=401)
                return response
    else:
        return render(request, 'airtime-order.html')


@login_required(login_url='signin')
def mobile_data(request):
    if request.method == "POST":
        d_pack = request.POST['d_pack']
        get_queryset = Utility.objects.filter(service_id=d_pack)
        data = serializers.serialize('json', get_queryset)
        #print(data)
        return HttpResponse(data, content_type="application/json")
    else:
        return render(request, 'data.html')
    


@login_required(login_url='signin')
def data_confirm(request):
    T = 6
    res = ''.join(random.choices(string.digits, k=T))
    txn = str(res)
    txt_id = "TX" + txn
    c_user_name = request.user.last_name
    c_user_id = request.user.id
    c_user_phone = request.user.phone
    c_email = request.user.email
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_email = request.user.email
    if request.method == "POST":
        d_pack = request.POST['d_pack']
        sel_pack = request.POST['sel_pack']
        m_no = request.POST['m_no']
        amount = Utility.objects.values('variation_amount').get(variation_code=sel_pack)['variation_amount']
        c_bal = Account.objects.all().get(user_id=c_user_id)

        url = "https://sandbox.vtpass.com/api/pay"

        payload = '{\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ d_pack +'\",\n\t\"billersCode\" : \"'+ m_no +'\",\n\t\"variation_code\" : \"'+ sel_pack +'\",\n\t\"amount\" : \"'+ str(amount) +'\",\n\t\"phone\" : \"'+ m_no +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6InZwdE5UdmNoYVhSVjZYQlJlcUVIQmc9PSIsInZhbHVlIjoieDBHc1BNaytJb29OM1BBOGg0c0lzM1dEa3NRRmRSSlk2TjBDZlcwZDNoREhNQkJrK0Z1VEpuckUydlpBWkJ1c001MHJpVGVOVWhrTklwYkRra3BjOHc9PSIsIm1hYyI6IjdjNTQ2N2UwNDFiNmFmZDNiM2ZjZTlkYjBkOGVjY2YyYWM5MTcxNjhjMGQ2NTlkZDg2NzI0ODNjOWUzMWE1NDEifQ%3D%3D'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            dat = d_dict[j]
            data_ = f.append(dat)
        data_resp = f[1]
        #print(data_resp)
        p_descr = data_resp['transactions']['product_name']
        trans_type = data_resp['transactions']['type']
        txf = data_resp['transactions']['transactionId']
        status = data_resp['transactions']['status']
        mno = data_resp['transactions']['unique_element']
        if status == "delivered":
            new = (c_bal.bal - float(amount))
            Account.objects.filter(user_id=c_user_id).update(bal=new)
            tx = Transactions(user_id=c_user_id, payment_ref=txt_id, payment_date=now, rec_email=c_email, 
            payment_desc=p_descr + "/" + mno + "/" + trans_type, payment_method="WALLET", trans_ref=txf, cur="NGN", amount=amount, payment_status="PAID")
            tx.save()
            subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
            html_content = render_to_string('mail/data.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': d_pack, 'trans_ref': txf})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return HttpResponse()
        else:
            response = HttpResponse(status=401)
            return response

@login_required(login_url='signin')
def power(request):
    if request.method == "POST":
        plan = request.POST['plan']
        sub = request.POST['sub']
        m_no = request.POST['m_no']

        url = "https://sandbox.vtpass.com/api/merchant-verify"

        payload = '{\n\t\n\t\"billersCode\" : '+ m_no +',\n\t\"serviceID\" : \"'+ sub +'\",\n\t\"type\": \"'+ plan +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6Im1vanQ3OTI5V2NpQWVSUG1nYTZIbWc9PSIsInZhbHVlIjoibWFTclwvUW5ibnFVb29iUTRFa0MxdjBwN1U0ZlduRjhmMmF3XC9XbWFZK21HN1VJQkM0YmRIWkZlQUh4bU9halhabjdnUzI5YWI3R3FjZEQ5TGtrQWd3UT09IiwibWFjIjoiNTQyODA4NzllODM4MTIwZTdkNzY0Nzg4ZTU5ZjI2Yzg3N2EwMzdjZWVmNGM4NDQ0MDdkNmFlMTlhMTI4NWI4MSJ9'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            tv = d_dict[j]
            s_tv = f.append(tv)
        p_resp = f[1]
        print(p_resp)
        if 'error' in p_resp:
            error = p_resp['error']
            messages.error(request, error)
            return redirect('power')
        else:
            if int(m_no) or m_no in p_resp.values():
                cus_name = p_resp['Customer_Name']
                if sub == "ibadan-electric" or sub == "jos-electric" or sub == "portharcourt-electric":
                    cus_name = p_resp['Customer_Name']
                    MeterNumber = p_resp['MeterNumber']
                    address = p_resp['Address']
                    return render(request, 'power.html', {'cus_name': cus_name, 'm_no': MeterNumber, 'address': address, 'plan': plan, 'sub': sub})
                elif p_resp['Customer_Name'] in p_resp.values():
                    m_no = p_resp['Meter_Number'] 
                    cus_dist = p_resp['Customer_District']
                    address = p_resp['Address']
                    return render(request, 'power.html', {'cus_name': cus_name, 'm_no': m_no, 
                    'cus_dist': cus_dist, 'address': address, 'plan': plan, 'sub': sub})
                else:
                    error = p_resp['error']
                    messages.error(request, error)
                    return redirect('power')
    else:
        return render(request, 'power.html')


@login_required(login_url='signin')
def power_confirm(request):
    T = 6
    res = ''.join(random.choices(string.digits, k=T))
    txn = str(res)
    txt_id = "TX" + txn

    c_user_id = request.user.id
    c_user_phone = request.user.phone
    c_user_name = request.user.last_name
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_email = request.user.email
    if request.method == "POST":
        plan = request.POST['plan']
        sub = request.POST['sub']
        amount = request.POST['amount']
        m_no = request.POST['m_no']
        c_bal = Account.objects.all().get(user_id=c_user_id)

        url = "https://sandbox.vtpass.com/api/pay"

        payload = '{\n\t\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ sub +'\",\n\t\"billersCode\" : \"'+ m_no +'\",\n\t\"variation_code\": \"'+ plan +'\",\n\t\"amount\" : \"'+ amount +'\",\n\t\"phone\": \"'+ c_user_phone +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6InZKWlwvMU5DYkxBV0NCNGI2dW1BXC9KQT09IiwidmFsdWUiOiJIbVRZTkkxYTh3ZXk4QzZnQkM2MCtZbVBtRkdha2tVYlVTNk9kWndoZXpVTlZmeGxRT3R2MkUzcWdERlFiQlBSeTBIWFhtb2dJdHB5NnhmdzdDUHp1dz09IiwibWFjIjoiMWEyNTVkNDk2YmQ4ZmU5M2MzOGE1YjJlZjZiM2Y4MDUxMGJmNWY4MDZkYjA4ZjExMjYxMDM1YTYzMzNkYmE3NiJ9'
        }

        response = requests.request("POST", url, headers=headers, data = payload)
        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            po = d_dict[j]
            s_po = f.append(po)
        po_resp = f[1]
        status = po_resp['transactions']['status']
        desc = po_resp['transactions']['product_name']
        tran_id = po_resp['transactions']['transactionId']
        #print(po_resp)
        code = f[6]
        #print(plan)
        if status == "delivered":
            if plan == "postpaid":
                new = (c_bal.bal - float(amount))
                Account.objects.filter(user_id=c_user_id).update(bal=new)
                tx = Transactions(user_id=c_user_id, payment_ref=txt_id, payment_date=now, rec_email=c_email, 
                payment_desc=desc, payment_method="WALLET", trans_ref=tran_id, amount=amount, cur="NGN", payment_status="PAID")

                elect = Electricity(owner_id=c_user_id, txn_id=txt_id, txn_ref=tran_id, desc=desc, 
                token="POSTPAID", date_purchase=now)
                tx.save()
                elect.save()

                #Buyer/User Email 
                subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
                html_content = render_to_string('mail/elect.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()

            #Sending of the Electricty Token

                subject, from_email, to = 'Electricity PIN/Token', EMAIL_FROM, c_email
                html_content = render_to_string('mail/elect2.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id, 'token': code})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return HttpResponse()
            else:
                new = (c_bal.bal - float(amount))
                Account.objects.filter(user_id=c_user_id).update(bal=new)
                tx = Transactions(user_id=c_user_id, payment_ref=txt_id, payment_date=now, rec_email=c_email, 
                payment_desc=desc, payment_method="WALLET", trans_ref=tran_id, amount=amount, cur="NGN", payment_status="PAID")

                elect = Electricity(owner_id=c_user_id, txn_id=txt_id, txn_ref=tran_id, desc=desc, 
                token=code, date_purchase=now)
                tx.save()
                elect.save()

                #Buyer/User Email 
                subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
                html_content = render_to_string('mail/elect.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()

            #Sending of the Electricty Token

                subject, from_email, to = 'Electricity PIN/Token', EMAIL_FROM, c_email
                html_content = render_to_string('mail/elect2.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': m_no, 'amt': amount, 'd_pack': sub, 'trans_ref': tran_id, 'token': code})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return HttpResponse()
        else:
            response = HttpResponse(status=401)
            return response

@login_required(login_url='signin')
def tv(request):
    U = 6
    res = ''.join(random.choices(string.digits, k=U))
    txn = str(res)
    txt_id = "TX" + txn
    c_email = request.user.email
    c_id = request.user.id
    c_user_id = request.user.id
    if request.method == "POST":
        sno = request.POST['sno']
        biller = request.POST['biller']
        url = "https://sandbox.vtpass.com/api/merchant-verify"

        payload = '{\n\t\n\t\"billersCode\" : '+ sno +',\n\t\"serviceID\" : \"'+ biller +'\"\n}'
        headers = {
        'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
        'Content-Type': 'application/json',
        'Cookie': 'laravel_session=eyJpdiI6IkFQYW1NZExRblhrQWhnMDRMb2IwY3c9PSIsInZhbHVlIjoiTnVrMURUeFIwbTlLeUpMZ25jTTB0eGkzeDVaZ1ZnQkRWMzFJS0lFQ3JqTGx3NzRSMXNxczdHVGlGS0xJRkFLbVF1akhxMms3TndwZUtLc0x1K205d3c9PSIsIm1hYyI6IjZkYTA3NDliOGE0OGZkNWY2MTliNTNlM2EzOWQyYjYyYmMyOWEyNGI4ZDA5NWZmMmNhNjJjZWYxYjU5NWIxNDEifQ%3D%3D'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        #print(response.text.encode('utf8'))
        d_dict = json.loads(response.text)
        f = []
        for j in d_dict:
            tv = d_dict[j]
            s_tv = f.append(tv)
        tv_resp = f[1]
        #print(tv_resp)
        status_code = f[0]
        bill = "startimes"
        if int(sno) or sno in tv_resp.values():
            cus_name = tv_resp['Customer_Name']
            #print(sno in tv_resp.values())
            if biller == bill:
                cus_id = tv_resp['Smartcard_Number']
                show = Utility.objects.filter(service_id=biller)
                return render(request, 'dstv.html', {'status_code': status_code, 'cus_name': cus_name, 'sno': sno, 'cus_id': cus_id, 'show': show})
            elif cus_name in tv_resp.values():
                cus_id = tv_resp['Customer_ID']
                show = Utility.objects.filter(service_id=biller)
                return render(request, 'dstv.html', {'status_code': status_code, 'cus_name': cus_name, 'sno': sno, 'cus_id': cus_id, 'show': show})
            else:
                return redirect('tv')
        else:
            messages.error(request, "Invalid")
            return redirect('tv')
    else:
        return render(request, 'dstv.html')


@login_required(login_url='signin')
def dstv_card_pay(request):
    c_user_id = request.user.id
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    if request.method == "GET":
        resp = request.GET.get('paymentReference')
        card = request.GET.get('card')
        print(resp)
        url = f"https://sandbox.monnify.com/api/v1/merchant/transactions/query?paymentReference={resp}"

        payload = {}
        headers = {
        'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy'
        }

        response = requests.request("GET", url, headers=headers, data = payload)

        r_dict = json.loads(response.text)
        
        e = []
        for i in r_dict:
            dt = r_dict[i]
            txn = e.append(dt)
        trans = e[3]
        #print(trans)
        #print(card)
        paymentRef = trans['paymentReference']
        p_method = trans['paymentMethod']
        amount = trans['amount']
        transRef = trans['transactionReference']
        rec_email = trans['customerEmail']
        p_status = trans['paymentStatus']
        p_desc = trans['paymentDescription']
        
        amt = int(amount)
        print(amt)
        if p_status == "PAID":
            url = "https://api.ravepay.co/v2/services/confluence"

            payload = '{\n  \"secret_key\": \"FLWSECK_TEST-0b93cce86006c4f85d7af034d51169eb-X\",\n  \"service\": \"fly_buy\",\n  \"service_method\": \"post\",\n  \"service_version\": \"v1\",\n  \"service_channel\": \"rave\",\n  \"service_payload\": {\n    \"Country\": \"NG\",\n    \"CustomerId\": \"'+ card +'\",\n    \"Reference\": \"'+ transRef +'\",\n    \"Amount\": '+ str(amt) +',\n    \"RecurringType\": 0,\n    \"IsAirtime\": false,\n    \"BillerName\": \"DSTV\"\n  }\n}'
            headers = {
            'Content-Type': 'application/json'
            }

            response = requests.request("POST", url, headers=headers, data = payload)
            print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                airt = d_dict[j]
                airtime = f.append(airt)
            air_resp = f[2]
            msg = air_resp['Message']
            tx = Transactions(user_id=c_user_id, payment_ref=paymentRef, payment_date=now, rec_email=rec_email, 
            payment_desc=p_desc, payment_method=p_method, trans_ref=transRef, amount=amount, cur="NGN", payment_status=p_status)
            tx.save()
            bal = Account.objects.all().get(user_id=c_user_id)
            return render(request, 'dstv_done.html', {'bal': bal, 'msg': msg})
        else:
            msg = "Fail Transaction!!!"
            bal = Account.objects.all().get(user_id=c_user_id)
            return render(request, 'dstv_done.html', {'bal': bal, 'msg': msg})


@login_required(login_url='signin')
def tv_wallet_pay(request):
    T = 6
    res = ''.join(random.choices(string.digits, k=T))
    txn = str(res)
    txt_id = "TX" + txn

    c_user_id = request.user.id
    c_user_phone = request.user.phone
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_email = request.user.email
    c_user_name = request.user.last_name
    if request.method == "POST":
        variation_code = request.POST['variation_code']
        sno = request.POST['sno']
        amount = Utility.objects.values('variation_amount').get(variation_code=variation_code)['variation_amount']
        service_id = Utility.objects.values('service_id').get(variation_code=variation_code)['service_id']
        amt = float(amount)
        act_bal = Account.objects.values('bal').get(user_id=c_user_id)['bal']
        new_bal = act_bal - amt
        if amt > act_bal:
            messages.error(request, "Insufficient Balance")
            return redirect('tv')
        else:
            url = "https://sandbox.vtpass.com/api/pay"

            payload = '{\n\t\n\t\"request_id\" : \"'+ txt_id +'\",\n\t\"serviceID\" : \"'+ service_id +'\",\n\t\"billersCode\" : \"'+ sno +'\",\n\t\"variation_code\": \"'+ variation_code +'\",\n\t\"amount\" : \"'+ str(amount) +'\",\n\t\"phone\": \"'+ c_user_phone +'\"\n}'
            headers = {
            'Authorization': 'Basic aWxlbW9iYXlvc2Ftc29uQGdtYWlsLmNvbToxMjM0NTY3OA==',
            'Content-Type': 'application/json',
            'Cookie': 'laravel_session=eyJpdiI6ImJOMSs1MHpYejlYeEQyV01TSEVSRWc9PSIsInZhbHVlIjoidE9KaDc4MFFQYVkyS3duTVBHQXVsVHpsRlcwbHV0SG5FZ2pJOHVtY3RMd3BJWGM4QW5RdU5rQ1hzSzQ5UFVodzRhUVE0RGFVeEFxK0dPUktqV1BSa0E9PSIsIm1hYyI6ImFhZGMzMDY0OGIyYmViM2QwNDAyMDYxOWMwYWIwOGM2MmYxYzZhN2IzYzE4YzcyOTkzY2JjOTU5NTIzNDViZGUifQ%3D%3D'
            }

            response = requests.request("POST", url, headers=headers, data = payload)

            #print(response.text.encode('utf8'))
            d_dict = json.loads(response.text)
            f = []
            for j in d_dict:
                airt = d_dict[j]
                airtime = f.append(airt)
            tv_resp = f[1]['transactions']
            status = tv_resp['status']
            p_name = tv_resp['product_name']
            cno = tv_resp['unique_element']
            desc = tv_resp['type']
            trans_id = tv_resp['transactionId']
            if status == "delivered":
                Account.objects.filter(user_id=c_user_id).update(bal=new_bal)
                tx = Transactions(user_id=c_user_id, payment_ref=txt_id, payment_date=now, rec_email=c_email, 
                payment_desc=desc + "/" + p_name + "/" + cno, payment_method="WALLET", trans_ref=trans_id, amount=amount, cur="NGN", payment_status="PAID")
                tx.save()
                # Sender Email Notification
                subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_email
                html_content = render_to_string('mail/dstv.html',
                                        {'last_name': c_user_name, 'date': now, 'reciever': desc + '/' + cno + '/' + p_name, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_id})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return HttpResponse()
            else:
                messages.error(request, "Fail")
                return redirect('tv')
    else:
        msg = "Fail Transaction!!"
        bal = Account.objects.all().get(user_id=c_user_id)
        return render(request, 'dstv_done.html', {'bal': bal, 'msg': msg})



@login_required(login_url='signin')
def transactions(request):
    c_id = request.user.id
    c_email = request.user.email
    bal = Account.objects.all().get(user_id=c_id)
    show = Transactions.objects.filter(Q(user_id=c_id) | Q(rec_email=c_email)).order_by('-id')
    paginator = Paginator(show, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'transactions.html', {'show': page_obj, 'bal': bal})



@login_required(login_url='signin')
def send_money_auth(request):
    U = 14
    res1 = ''.join(random.choices(string.digits, k=U))
    txn1 = str(res1)
    N = 6
    res2 = ''.join(random.choices(string.digits, k=N))
    txn2 = str(res2)
    trans_ref = "WLTR|" + txn1 + "|" + txn2
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    H = 6
    res = ''.join(random.choices(string.digits, k=H))
    txn = str(res)
    txt_id = "TX" + txn
    c_id = request.user.id
    c_user_email = request.user.email
    c_user_name = request.user.last_name
    if request.method == 'POST':
        r_email = request.POST['r_email']
        amount = request.POST['amount']
        s_acct = request.POST['s_acct']
        r_acct = request.POST['r_acct']
        r_name = UserModel.objects.values('last_name').get(email=r_email)['last_name']
        s_all = UserModel.objects.all().get(id=c_id)
        all_d = Account.objects.all().get(user_id=c_id)
        if request.user.email == r_email:
            response = HttpResponse(status=401)
            return response
        else:
            if s_acct == "NGN" and r_acct == "NGN":
                if (float(amount)) > all_d.bal:
                    response = HttpResponse(status=401)
                    return response
                else:
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.bal

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(amount) + r_bal.bal)
                    new2 = (s_bal - float(amount))

                    Account.objects.filter(user_id=r_info.id).update(bal=new1)
                    Account.objects.filter(user_id=c_id).update(bal=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "NGN/NGN/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="NGN", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "NGN" and r_acct == "USD":
                if (float(amount)) > all_d.bal:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=NGN/USD&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.bal

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.usd)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(usd=new)
                    Account.objects.filter(user_id=c_id).update(bal=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "NGN/USD/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="NGN", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "NGN" and r_acct == "EUR":
                if (float(amount)) > all_d.bal:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=NGN/EUR&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.bal

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.eur)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(eur=new)
                    Account.objects.filter(user_id=c_id).update(bal=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "NGN/EUR/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="NGN", payment_status="PAID")
                    tx.save()
                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "NGN" and r_acct == "GBP":
                if (float(amount)) > all_d.bal:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=NGN/GBP&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.bal

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.gbp)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(gbp=new)
                    Account.objects.filter(user_id=c_id).update(bal=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "NGN/GBP/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="NGN", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'NGN/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "USD" and r_acct == "NGN":
                if (float(amount)) > all_d.usd:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.usd

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.bal)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(bal=new)
                    Account.objects.filter(user_id=c_id).update(usd=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "USD/NGN/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="USD", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "USD" and r_acct == "USD":
                if (float(amount)) > all_d.usd:
                    response = HttpResponse(status=401)
                    return response
                else:
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.usd

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(amount) + r_bal.usd)
                    new2 = (s_bal - float(amount))

                    Account.objects.filter(user_id=r_info.id).update(usd=new1)
                    Account.objects.filter(user_id=c_id).update(usd=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "USD/USD/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="USD", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "USD" and r_acct == "EUR":
                if (float(amount)) > all_d.usd:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/EUR&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.usd

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.eur)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(eur=new)
                    Account.objects.filter(user_id=c_id).update(usd=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "USD/EUR/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="USD", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "USD" and r_acct == "GBP":
                if (float(amount)) > all_d.usd:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=USD/GBP&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.usd

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.gbp)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(eur=new)
                    Account.objects.filter(user_id=c_id).update(usd=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "USD/GBP/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="USD", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'USD/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "EUR" and r_acct == "NGN":
                if (float(amount)) > all_d.eur:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.eur

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.bal)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(bal=new)
                    Account.objects.filter(user_id=c_id).update(eur=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "EUR/NGN/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="EUR", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "EUR" and r_acct == "USD":
                if (float(amount)) > all_d.eur:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/USD&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.eur

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.usd)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(usd=new)
                    Account.objects.filter(user_id=c_id).update(eur=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "EUR/USD/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="EUR", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "EUR" and r_acct == "EUR":
                if (float(amount)) > all_d.eur:
                    response = HttpResponse(status=401)
                    return response
                else:
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.eur

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(amount) + r_bal.eur)
                    new2 = (s_bal - float(amount))

                    Account.objects.filter(user_id=r_info.id).update(eur=new1)
                    Account.objects.filter(user_id=c_id).update(eur=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "EUR/EUR/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="EUR", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "EUR" and r_acct == "GBP":
                if (float(amount)) > all_d.eur:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=EUR/GBP&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.eur

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.gbp)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(gbp=new)
                    Account.objects.filter(user_id=c_id).update(eur=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "EUR/GBP/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="EUR", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'EUR/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "GBP" and r_acct == "NGN":
                if (float(amount)) > all_d.gbp:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/NGN&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.gbp

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.bal)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(bal=new)
                    Account.objects.filter(user_id=c_id).update(gbp=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "GBP/NGN/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="GBP", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/NGN', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "GBP" and r_acct == "USD":
                if (float(amount)) > all_d.gbp:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/USD&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.gbp

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.usd)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(usd=new)
                    Account.objects.filter(user_id=c_id).update(gbp=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "GBP/USD/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="GBP", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/USD', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "GBP" and r_acct == "EUR":
                if (float(amount)) > all_d.gbp:
                    response = HttpResponse(status=401)
                    return response
                else:
                    url = 'https://fcsapi.com/api-v2/forex/converter?symbol=GBP/EUR&amount='+ amount +'&access_key=X4OjsrrAQCU9HtGrffJ3Qkd7dsQdGMASohwJmuevLJ34YfS74a'

                    payload = {}
                    headers = {
                    'Cookie': 'sess=ff60072be362382a30f8c1161633640da1f403ce'
                    }

                    response = requests.request("POST", url, headers=headers, data = payload)

                    #print(response.text.encode('utf8'))
                    r_dict = json.loads(response.text)
        
                    e = []
                    for i in r_dict:
                        dt = r_dict[i]
                        txn = e.append(dt)
                    trans = e[3]
                    total = trans['total']
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.gbp

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(total) + r_bal.eur)
                    new2 = (s_bal - float(amount))
                    new = (str(round(new1, 2)))
                    Account.objects.filter(user_id=r_info.id).update(eur=new)
                    Account.objects.filter(user_id=c_id).update(gbp=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "GBP/EUR/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="GBP", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/EUR', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            elif s_acct == "GBP" and r_acct == "GBP":
                if (float(amount)) > all_d.gbp:
                    response = HttpResponse(status=401)
                    return response
                else:
                    # Sender Info
                    bal = Account.objects.all().get(user_id=c_id)
                    s_bal = bal.gbp

                    # Receiver Info
                    r_info = UserModel.objects.all().get(email=r_email)
                    r_bal = Account.objects.all().get(user_id=r_info.id)
        
                    new1 = (float(amount) + r_bal.gbp)
                    new2 = (s_bal - float(amount))

                    Account.objects.filter(user_id=r_info.id).update(gbp=new1)
                    Account.objects.filter(user_id=c_id).update(gbp=new2)

                    tx = Transactions(user_id=c_id, payment_ref=txt_id, payment_date=now, rec_email=r_email, 
                    payment_desc="WLT|" + s_all.last_name + "|TO|" + "GBP/GBP/" + r_name, payment_method="WALLET", trans_ref=trans_ref, amount=amount, cur="GBP", payment_status="PAID")
                    tx.save()

                    # Sender Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
                    html_content = render_to_string('mail/money.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #Reciever Email Notification
                    subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
                    html_content = render_to_string('mail/rmoney.html',
                                            {'last_name': c_user_name, 'date': now, 'reciever': r_info.last_name, 'amt': amount, 'cur': 'GBP/GBP', 'trans_ref': trans_ref, 'total': '/' + total})
                    text_content = strip_tags(html_content)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return HttpResponse()
            else:
                response = HttpResponse(status=401)
                return response
    else:
        all_b = Account.objects.all().get(user_id=c_id)
        return render(request, 'send_money.html', {'all_b': all_b})


@login_required(login_url='signin')
def withdraw(request):
    c_id = request.user.id
    if request.method == "POST":
        acct_no = request.POST['acct_no']
        bank = request.POST['bank']
        amount = request.POST['amount']

        url = f"https://sandbox.monnify.com/api/v1/disbursements/account/validate?accountNumber={acct_no}&bankCode={bank}"

        payload = {}
        headers= {}

        response = requests.request("GET", url, headers=headers, data = payload)

        r_dict = json.loads(response.text)

        acc = []
        for i in r_dict:
            bnk = r_dict[i]
            acc.append(bnk)
        status = acc[1]
        if status == "success":
            ac_d = acc[3]
            acct_num = ac_d['accountNumber']
            acct_name = ac_d['accountName']
            bnk_code = ac_d['bankCode']

            bank_name = Banks.objects.values('name').get(code=bnk_code)['name']
            return render(request, 'withdraw_confirm.html', {'acct_num': acct_num, 'acct_name': acct_name, 'bnk_code': bnk_code, 
            'amount': amount, 'bank_name': bank_name})
        else:
            messages.error(request, "Invalid Account Details")
            return redirect('withdraw')
    bank = Banks.objects.filter()
    return render(request, 'withdraw.html', {'bank': bank})


@login_required(login_url='signin')
def withdraw_confirm(request):
    U = 14
    res1 = ''.join(random.choices(string.digits, k=U))
    txn1 = str(res1)
    N = 6
    res2 = ''.join(random.choices(string.digits, k=N))
    txn2 = str(res2)
    trans_ref = "WDRT|" + txn1 + "|" + txn2
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    c_id = request.user.id
    U = 6
    res = ''.join(random.choices(string.digits, k=U))
    txn = str(res)
    txt_id = "TX" + txn
    c_user_name = request.user.last_name
    r_email = request.user.email
    if request.method == "POST":
        amount = request.POST['amount']
        acct_num = request.POST['acct_num']
        bnk_code = request.POST['bnk_code']

        bals = Account.objects.all().get(user_id=c_id)
        if float(amount) > bals.bal:
            response = HttpResponse(status=401)
            return response
        else:
            url = "https://sandbox.monnify.com/api/v1/disbursements/single"

            payload = '{\n    \"amount\": '+ amount +',\n    \"reference\":\"'+ txt_id +'\",\n    \"narration\":\"Withdraw\",\n    \"bankCode\": \"' + bnk_code +'\",\n    \"accountNumber\": \"'+ acct_num +'\",\n    \"currency\": \"NGN\",\n    \"walletId\": \"654CAB2118124760A659C787B2AA38E8\"\n}'
            headers = {
            'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy',
            'Content-Type': 'application/json'
            }

            response = requests.request("POST", url, headers=headers, data = payload)

            r_dict = json.loads(response.text)

            resp = []
            for i in r_dict:
                bnk = r_dict[i]
                resp.append(bnk)
            res = resp[3]
            amount = res['amount']
            ref = res['reference']
            status = res['status']

            # Customer Info

            new = (bals.bal - float(amount))
            Account.objects.filter(user_id=c_id).update(bal=new)
            tx = Transactions(user_id=c_id, payment_ref=ref, payment_date=now, rec_email=r_email, 
            payment_desc="BANK TRANSFER", payment_method="TRANSFER", trans_ref=trans_ref, amount=amount, cur="NGN", payment_status=status)
            tx.save()
            # Sender Email Notification
            subject, from_email, to = 'Successful Transaction', EMAIL_FROM, r_email
            html_content = render_to_string('mail/transfer.html',
                                    {'last_name': c_user_name, 'date': now, 'reciever': 'TRF/' + acct_num, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return HttpResponse()


@require_POST
@csrf_exempt
def confirm_transfer(request):
    U = 6
    res = ''.join(random.choices(string.digits, k=U))
    txn = str(res)
    txt_id = "TX" + txn
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    request_json = request.body.decode('utf-8')
    body = json.loads(request_json)
    trans_ref = body['transactionReference']
    amount = body['amountPaid']
    method = body['paymentMethod']
    email = body['customer']['email']
    name = body['customer']['name']
    receiver = body['customer']['name']
    desc = body['paymentDescription']
    status = body['paymentStatus']
    t_type = body['product']['type']
    p_ref = body['paymentReference']

    max_charge = 300
    charge = (float(amount) * (1.2 / 100))
    final1 = float(amount) - float(max_charge)
    final2 = float(amount) - float(charge)

    if float(charge) >= float(max_charge):
        if t_type == "INVOICE":
            user_id = Invoice.objects.values('user_id').get(trans_ref=p_ref)['user_id']
            bal = Account.objects.values('bal').get(user_id=user_id)['bal']
            c_user_name = UserModel.objects.values('last_name').get(id=user_id)['last_name']
            c_user_email = UserModel.objects.values('email').get(id=user_id)['email']
            new = (float(final1) + float(bal))
            Account.objects.filter(user_id=user_id).update(bal=new)
            Invoice.objects.filter(trans_ref=p_ref).update(status=status)
            tran = Transactions(user_id=user_id, payment_ref=p_ref, payment_date=now, rec_email=email, payment_desc="INVOICE|PD|" + name, payment_method=method, 
            trans_ref=trans_ref, amount=final1, cur="NGN", payment_status=status)
            tran.save()
            # Sender Email Notification
            subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
            html_content = render_to_string('mail/paid_invoice.html',
                                {'last_name': c_user_name, 'date': now, 'reciever': 'INVOICE|PD|' + name, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref, 'p_ref': p_ref, 'charge': max_charge})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return HttpResponse(request_json, status=200)
        elif method == "CARD":
            return HttpResponse(request_json, status=200)
        else:
            payer = body['accountDetails']['accountName']
            user_id = UserModel.objects.all().get(email=email)
            bal = Account.objects.values('bal').get(user_id=user_id.id)['bal']

            new = (float(final1) + float(bal))
            Account.objects.filter(user_id=user_id.id).update(bal=new)

            tran = Transactions(user_id=user_id.id, payment_ref=txt_id, payment_date=now, rec_email=email, payment_desc="CREDIT|TRF|" + payer, payment_method=method, trans_ref=trans_ref, 
            amount=final1, cur="NGN", payment_status=status)
            tran.save()
            subject, from_email, to = 'Successful Transaction', EMAIL_FROM, email
            html_content = render_to_string('mail/bank_transfer.html',
                                {'last_name': user_id.last_name, 'date': now, 'reciever': 'CREDIT|TRF|' + payer, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref, 'charge': max_charge})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return HttpResponse(request_json, status=200)
    else:
        if t_type == "INVOICE":
            user_id = Invoice.objects.values('user_id').get(trans_ref=p_ref)['user_id']
            bal = Account.objects.values('bal').get(user_id=user_id)['bal']
            c_user_name = UserModel.objects.values('last_name').get(id=user_id)['last_name']
            c_user_email = UserModel.objects.values('email').get(id=user_id)['email']
            new = (float(final2) + float(bal))
            Account.objects.filter(user_id=user_id).update(bal=new)
            Invoice.objects.filter(trans_ref=p_ref).update(status=status)
            tran = Transactions(user_id=user_id, payment_ref=p_ref, payment_date=now, rec_email=email, payment_desc="INVOICE|PD|" + name, payment_method=method, 
            trans_ref=trans_ref, amount=final2, cur="NGN", payment_status=status)
            tran.save()
            # Sender Email Notification
            subject, from_email, to = 'Successful Transaction', EMAIL_FROM, c_user_email
            html_content = render_to_string('mail/paid_invoice.html',
                                {'last_name': c_user_name, 'date': now, 'reciever': 'INVOICE|PD|' + name, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref, 'p_ref': p_ref, 'charge': charge})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return HttpResponse(request_json, status=200)
        elif method == "CARD":
            return HttpResponse(request_json, status=200)
        else:
            payer = body['accountDetails']['accountName']
            user_id = UserModel.objects.all().get(email=email)
            bal = Account.objects.values('bal').get(user_id=user_id.id)['bal']

            new = (float(final2) + float(bal))
            Account.objects.filter(user_id=user_id.id).update(bal=new)

            tran = Transactions(user_id=user_id.id, payment_ref=txt_id, payment_date=now, rec_email=email, payment_desc="CREDIT|TRF|" + payer, payment_method=method, trans_ref=trans_ref, 
            amount=final2, cur="NGN", payment_status=status)
            tran.save()
            subject, from_email, to = 'Successful Transaction', EMAIL_FROM, email
            html_content = render_to_string('mail/bank_transfer.html',
                                {'last_name': user_id.last_name, 'date': now, 'reciever': 'CREDIT|TRF|' + payer, 'amt': amount, 'cur': 'NGN', 'trans_ref': trans_ref, 'charge': charge})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return HttpResponse(request_json, status=200)


@login_required(login_url='signin')
def create_invoice(request):
    U = 6
    res = ''.join(random.choices(string.digits, k=U))
    txn = str(res)
    txt_id = "IV" + txn
    c_id = request.user.id
    c_user = request.user.last_name
    if request.method == "POST":
        item1 = request.POST['item1']
        item2 = request.POST['item2']
        item3 = request.POST['item3']
        price1 = request.POST['price1']
        price2 = request.POST['price2']
        price3 = request.POST['price3']
        desc = request.POST['desc']
        date = request.POST['date']
        time = request.POST['time']
        p_type = request.POST['p_type']
        c_name = request.POST['c_name']
        c_email = request.POST['c_email']
        u_time = time + ":00"
        final_date = date + " " + u_time
        total = (float(price1) + float(price2) + float(price3))
        final_total = str(total)
        
        url = "https://sandbox.monnify.com/api/v1/invoice/create"

        payload = '{\n    \"amount\": \"'+ final_total +'\",\n    \"invoiceReference\": \"'+ txt_id +'\",\n    \"description\": \"'+ desc +'\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"2917634474\",\n    \"customerEmail\": \"'+ c_email +'\",\n    \"customerName\": \"'+ c_name +'\",\n    \"expiryDate\": \"'+ final_date +'\",\n    \"paymentMethods\": [\"'+ p_type +'\"],\n    \"redirectUrl\": \"http://' + request.get_host() +'/user/invoice_success\"\n}'
        headers = {
        'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy',
        'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data = payload)

        r_dict = json.loads(response.text)

        resp = []
        for i in r_dict:
            bnk = r_dict[i]
            resp.append(bnk)
        res = resp[3]
        if resp[0] == True:
            if p_type == "ACCOUNT_TRANSFER":
                invoiceRef = res['invoiceReference']
                status = res['invoiceStatus']
                checkoutUrl = res['checkoutUrl']
                acct_no = res['accountNumber']
                acct_name = res['accountName']
                bank_name = res['bankName']
                bank_code = res['bankCode']
                inv = Invoice(user_id=c_id, p_type=p_type, trans_ref=invoiceRef, due_date=final_date, desc=desc, cust_name=c_name, cus_email=c_email, item1=item1, item2=item2, item3=item3,
                price1=price1, price2=price2, price3=price3, total=total, acct_no=acct_no, acct_name=acct_name, bank_name=bank_name, bank_code=bank_code, checkout_url=checkoutUrl, status=status)
                inv.save()
                subject, from_email, to = 'Invoice', EMAIL_FROM, c_email
                html_content = render_to_string('mail/invoice.html',
                                {'sender': c_user, 'cur': 'NGN', 'bank_name': bank_name, 'acct_name': acct_name, 'acct_no': acct_no, 
                                'bank_code': bank_code, 'checkoutUrl': checkoutUrl, 'final_date': final_date, 'total': total})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return render(request, 'invoice.html', {'bank_name': bank_name, 'acct_name': acct_name, 'acct_no': acct_no, 'bank_code': bank_code, 'checkoutUrl': checkoutUrl , 'item1': item1, 'item2': item2, 'item3': item3,
                    'price1': price1, 'price2': price2, 'price3': price3,
                    'desc': desc, 'p_type': p_type, 'c_name': c_name, 'txt_id': txt_id,
                    'c_email': c_email, 'final_date': final_date, 'total': total})
            else:
                invoiceRef = res['invoiceReference']
                status = res['invoiceStatus']
                checkoutUrl = res['checkoutUrl']
                inv = Invoice(user_id=c_id, p_type=p_type, trans_ref=invoiceRef, due_date=final_date, desc=desc, cust_name=c_name, cus_email=c_email, item1=item1, item2=item2, item3=item3,
                price1=price1, price2=price2, price3=price3, total=total, acct_no="CARD", acct_name="CARD", bank_name="CARD", bank_code="CARD", checkout_url=checkoutUrl, status=status)
                inv.save()
                subject, from_email, to = 'Invoice', EMAIL_FROM, c_email
                html_content = render_to_string('mail/invoice.html',
                                {'sender': c_user, 'cur': 'NGN', 'bank_name': 'CARD', 'acct_name': 'CARD', 'acct_no': 'CARD', 
                                'bank_code': 'CARD', 'checkoutUrl': checkoutUrl, 'final_date': final_date, 'total': total})
                text_content = strip_tags(html_content)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                return render(request, 'invoice.html', {'bank_name': "CARD", 'acct_name': "CARD", 'acct_no': "CARD", 'bank_code': "CARD", 'checkoutUrl': checkoutUrl , 'item1': item1, 'item2': item2, 'item3': item3,
                    'price1': price1, 'price2': price2, 'price3': price3,
                    'desc': desc, 'p_type': p_type, 'c_name': c_name, 'txt_id': txt_id,
                    'c_email': c_email, 'final_date': final_date, 'total': total})
        else:
            messages.error(request, "Error while attempting invoice creation!")
            return redirect('create_invoice')
    else:
        print(request.get_host())
        return render(request, 'invoice_create.html')


@login_required(login_url='signin')
def all_invoice(request):
    c_id = request.user.id
    show = Invoice.objects.filter(user_id=c_id).order_by('-id')
    paginator = Paginator(show, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'all_invoice.html', {'show': page_obj})



@csrf_exempt
def invoice_success(request):
    base_date_time = datetime.now()
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M %p"))
    if request.method == "GET":
        resp = request.GET.get('paymentReference')
        print(resp)
        url = f"https://sandbox.monnify.com/api/v1/merchant/transactions/query?paymentReference={resp}"

        payload = {}
        headers = {
        'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy'
        }

        response = requests.request("GET", url, headers=headers, data = payload)

        r_dict = json.loads(response.text)
        
        e = []
        for i in r_dict:
            dt = r_dict[i]
            txn = e.append(dt)
        trans = e[3]
        #print(trans)
        paymentRef = trans['paymentReference']
        p_method = trans['paymentMethod']
        amount = trans['amount']
        transRef = trans['transactionReference']
        rec_email = trans['customerEmail']
        p_status = trans['paymentStatus']
        p_desc = trans['paymentDescription']
        customerName = trans['customerName']
        user_id = Invoice.objects.values('user_id').get(trans_ref=paymentRef)['user_id']

        user_bal = Account.objects.values('bal').get(user_id=user_id)['bal']

        new = (float(user_bal) + float(amount))

        Account.objects.filter(user_id=user_id).update(bal=new)
        Invoice.objects.filter(trans_ref=paymentRef).update(status=p_status)

        create_trans_rec = Transactions(user_id=user_id, payment_ref=paymentRef, payment_date=now, rec_email=rec_email, 
        payment_desc="INVOICE Paid by " + customerName, payment_method=p_method, trans_ref=transRef, amount=amount, cur="NGN", payment_status=p_status)
        create_trans_rec.save()
    return render(request, 'invoice_success.html')


@login_required(login_url='signin')
def print_invoice(request, id):
    all_inv = Invoice.objects.all().get(id=id)
    return render(request, 'print_invoice.html', {'all_inv': all_inv})


@login_required(login_url='signin')
def cancel_invoice(request):
    if request.method == "POST":
        ref = request.POST['ref']
        print(ref)
        stat = Invoice.objects.all().get(trans_ref=ref)
        print(stat.status)
        if stat.status == "PAID":
            messages.error(request, "PAID invoice cannot be cancelled")
            return redirect(all_invoice)
        if stat.status == "CANCELLED":
            messages.error(request, "Invoice already CANCELLED")
            return redirect(all_invoice)
        else:
            url = f"https://sandbox.monnify.com/api/v1/invoice/{ref}/cancel"

            payload = {}
            headers = {
            'Authorization': 'Basic TUtfVEVTVF84VUJYR0tURlNCOkVOUkM0RkRZS1NUVVlRS0E1M1lQWEJGTFVGWFdZSEcy'
            }

            response = requests.request("DELETE", url, headers=headers, data = payload)

            r_dict = json.loads(response.text)

            resp = []
            for i in r_dict:
                bnk = r_dict[i]
                resp.append(bnk)
            res = resp[3]
            invoiceStatus = res['invoiceStatus']
            Invoice.objects.filter(trans_ref=ref).update(status=invoiceStatus)
            messages.success(request, f"You have succesfully cancelled invoice with ref No {ref}")
            return redirect(all_invoice)


@login_required(login_url='signin')
def receive_money_form(request):
    user_id = request.user.id
    first_name = request.user.first_name
    last_name = request.user.last_name
    email = request.user.email
    phone = request.user.phone
    base_date_time = datetime.now()
    U = 8
    res = ''.join(random.choices(string.digits, k=U))
    app_id = str(res)
    now = (datetime.strftime(base_date_time, "%Y-%m-%d %H:%M"))
    ex = ReceiveMoneyForm.objects.filter(user_id=user_id).exists()
    if ex:
        all_data = ReceiveMoneyForm.objects.all().get(user_id=user_id)
        return render(request, 'recieve_money_form.html', {'ex': ex, 'all_data': all_data})
    else:
        if request.method == "POST":
            address = request.POST['address']
            dob = request.POST['dob']
            m_identity = request.POST['m_identity']
            idno = request.POST['idno']
            app_form = ReceiveMoneyForm(user_id=user_id, first_name=first_name, last_name=last_name, email=email, phone=phone, address=address, 
            m_identity=m_identity, id_no=idno, app_id=app_id, date_applied=now, dob=dob, app_status='0')
            app_form.save()
            subject, from_email, to = 'Application Successful', EMAIL_FROM, email
            html_content = render_to_string('mail/intl_form.html',
                                {'last_name': last_name, 'date': now, 'app_id': app_id})
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            return HttpResponse()
        else:
            return render(request, 'recieve_money_form.html', {'ex': ex})


@login_required(login_url='signin')
def trans_reciept(request, id):
    data = Transactions.objects.all().get(payment_ref=id)
    return render(request, 'trans_reciept.html', {'data': data})