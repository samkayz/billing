from django.urls import path
from . import views


urlpatterns = [
    path('signin', views.signin, name='signin'),
    path('signup', views.signup, name='signup'),
    path('logout', views.logout, name='logout'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('setting', views.setting, name='setting'),
    path('transactions', views.transactions, name='transactions'),
    path('airtime_order', views.airtime_order, name='airtime_order'),
    path('mobile_data', views.mobile_data, name='mobile_data'),
    path('data_confirm', views.data_confirm, name='data_confirm'),
    path('power', views.power, name='power'),
    path('power_confirm', views.power_confirm, name='power_confirm'),
    #path('airtime_card_pay', views.airtime_card_pay, name='airtime_card_pay'),
    path('tv', views.tv, name='tv'),
    path('dstv_card_pay', views.dstv_card_pay, name='dstv_card_pay'),
    path('tv_wallet_pay', views.tv_wallet_pay, name='tv_wallet_pay'),
    # path('send_money', views.send_money, name='send_money'),
    path('send_money_auth', views.send_money_auth, name='send_money_auth'),
    path('withdraw', views.withdraw, name='withdraw'),
    path('withdraw_confirm', views.withdraw_confirm, name='withdraw_confirm'),
    path('add_account', views.add_account, name='add_account'),
    path('confirm_transfer', views.confirm_transfer),
    path('create_invoice', views.create_invoice, name='create_invoice'),
    path('all_invoice', views.all_invoice, name='all_invoice'),
    path('invoice_success', views.invoice_success, name='invoice_success'),
    path('print_invoice/<id>', views.print_invoice, name='print_invoice'),
    path('cancel_invoice', views.cancel_invoice, name='cancel_invoice'),
    path('receive_money_form', views.receive_money_form, name='receive_money_form'),
    path('trans_reciept/<id>', views.trans_reciept, name='trans_reciept'),
]