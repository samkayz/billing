from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.contrib.sessions.models import Session


class User(AbstractUser):
    phone = models.CharField(verbose_name='phone', max_length=255)
    email = models.CharField(verbose_name='email', max_length=255, unique=True)
    acct_no = models.CharField(verbose_name='acct_no', max_length=50)
    bank_name = models.CharField(verbose_name='bank_name', max_length=100)
    acct_name = models.CharField(verbose_name='acct_name', max_length=255)
    bank_code = models.CharField(verbose_name='bank_code', max_length=50)
    acct_type = models.IntegerField()
    date_registered = models.CharField(verbose_name='date_registered', max_length=100)
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', 'last_name']
    USERNAME_FIELD = 'email'

    class Meta:
        db_table = 'user'

    def get_username(self):
        return self


class ReservedAccount(models.Model):
    acct_no = models.CharField(max_length=100)
    bank_name = models.CharField(max_length=100)
    bank_code = models.CharField(max_length=100)
    reserve_ref = models.CharField(max_length=100)
    acct_type = models.CharField(max_length=100)
    acct_ref = models.CharField(max_length=100)
    acct_name = models.CharField(max_length=100)
    c_email = models.EmailField()
    status = models.CharField(max_length=50)
    cur = models.CharField(max_length=10)

    class Meta:
        db_table = 'reserved_account'

class UserSession(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    session = models.OneToOneField(Session, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_session'


class Account(models.Model):
    user_id = models.IntegerField()
    bal = models.FloatField()
    usd = models.FloatField()
    eur = models.FloatField()
    gbp = models.FloatField()

    class Meta:
        db_table = 'account'


class Transactions(models.Model):
    user_id = models.IntegerField()
    payment_ref = models.CharField(max_length=100)
    payment_date = models.CharField(max_length=100)
    rec_email = models.EmailField()
    payment_desc = models.CharField(max_length=255)
    payment_method = models.CharField(max_length=100)
    trans_ref = models.CharField(max_length=255)
    amount = models.FloatField()
    cur = models.CharField(max_length=20)
    payment_status = models.CharField(max_length=100)


    class Meta:
        db_table = 'transactions'



class VirtualPin(models.Model):
    pin = models.CharField(max_length=100)
    pin_amount = models.FloatField()
    pin_status = models.IntegerField()


    class Meta:
        db_table = 'virtual_pin'



class UsedPin(models.Model):
    pin_code = models.CharField(max_length=100)
    pin_amount = models.FloatField()
    smart_card = models.CharField(max_length=100)
    user_email = models.EmailField()
    date_used = models.CharField(max_length=100)

    class Meta:
        db_table = 'used_pin'


class Banks(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=10)

    class Meta:
        db_table = 'banks'



class BankAccount(models.Model):
    user_id = models.IntegerField()
    account_no = models.CharField(max_length=100)
    bank_code = models.CharField(max_length=100)
    bank_name = models.CharField(max_length=100)

    class Meta:
        db_table = 'bank_account'


class Invoice(models.Model):
    user_id = models.IntegerField()
    trans_ref = models.CharField(max_length=100)
    due_date = models.CharField(max_length=100)
    p_type = models.CharField(max_length=100)
    desc = models.TextField()
    cust_name = models.CharField(max_length=255)
    cus_email = models.EmailField()
    item1 = models.TextField()
    price1 = models.FloatField()
    item2 = models.TextField()
    price2 = models.FloatField()
    item3 = models.TextField()
    price3 = models.FloatField()
    total = models.FloatField()
    acct_no = models.CharField(max_length=100)
    acct_name = models.CharField(max_length=100)
    bank_name =models.CharField(max_length=100)
    bank_code = models.CharField(max_length=10)
    checkout_url = models.URLField()
    status = models.CharField(max_length=10)

    class Meta:
        db_table = 'invoice'


class ReceiveMoneyForm(models.Model):
    user_id = models.IntegerField()
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=100)
    address = models.TextField()
    dob = models.CharField(max_length=100)
    m_identity = models.CharField(max_length=100)
    id_no = models.CharField(max_length=100)
    app_id = models.CharField(max_length=100)
    date_applied = models.CharField(max_length=100)
    app_status = models.IntegerField()

    class Meta:
        db_table = 'receive_money_form'


class Utility(models.Model):
    service_name = models.CharField(max_length=100)
    service_id = models.CharField(max_length=100)
    variation_code = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    variation_amount = models.FloatField()

    class Meta:
        db_table = 'utility'


class Electricity(models.Model):
    owner_id = models.IntegerField()
    txn_id = models.CharField(max_length=100)
    txn_ref = models.CharField(max_length=100)
    desc = models.CharField(max_length=100)
    token = models.CharField(max_length=100)
    date_purchase = models.CharField(max_length=100)

    class Meta:
        db_table = 'electricity'


        
